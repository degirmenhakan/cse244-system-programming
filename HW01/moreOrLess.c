/*********************************************************
*
*		 	Hakan DEGIRMEN
*		      101044057
*
*		DATE: March 07 2015 Saturday
*
*		Course: CSE244     HW: 01
***********************************************************/

/**********************************************************
*	The program is sample more and less command in Unix.
***********************************************************/

/**********************************************************
*						COMPILER & RUN
*					_____________________
*
*		gcc -c moreOrLess.c
*		gcc moreOrLess.o -o moreOrLess
*		./moreOrLess <sfileName> <inumberOfsline>
**********************************************************/

/******************************
*		LIBRARIES
*******************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>

/*******************
*	Macro Variable
********************/
#define sbufferSIZE 1024 

/**********************************
*			FUNCTIONS
***********************************/
char getch();
void usage(int argc);
int readFile(FILE * fpsourceFile , int inumberOfsline);


/**************************************
*	DRIVER FUNCTION MAIN
*	@param int argc  num of argumans
*	@param char** argv  argumans
*	@return int success
***************************************/
int main(int argc,char **argv){

	int inumberOfsline,icurrentsline =0,i,icounter;
	char sbuffer[2048][255];
	char sline[255];
	char sfileName[255];
	int istatus=0;
	char ckey = 27;
	FILE *fpsourceFile;



	if(argc == 1){
		usage(1);
		return -1;
	}
	else if(argc == 2){
		usage(2);
		return -1;
	}else if(argc>3){
		usage(4);
		return -1;
	}
	
	strcpy(sfileName,argv[1]);
	inumberOfsline = atoi(argv[2]);

	fpsourceFile = fopen(sfileName,"r");
	if(fpsourceFile == NULL){
		fprintf(stderr,"Cannot open file.\nProgram is closing.\n");
		return -1;

	}
	if(inumberOfsline<0){
		fprintf(stderr,"Number of sline cannot be negative.\nProgram is closing.\n");
		return -1;


	}
	while(istatus != -1){
		switch(ckey){
			case 27:		/*Escape*/
				istatus = readFile(fpsourceFile,inumberOfsline);
				icurrentsline += inumberOfsline;
				break;
			case 10:		/*Enter*/
				istatus = readFile(fpsourceFile,1);
				icurrentsline += 1;
				break;
			case 115:	/*Down S charackter*/
			case 66:
				if(icurrentsline>inumberOfsline){		
					system("clear");
					fseek( fpsourceFile, 0, SEEK_SET);
					istatus = readFile(fpsourceFile,++icurrentsline);
					break;
				}
			case 97:/*Up A charackter*/
			case 65:		
				system("clear");
					if(icurrentsline>inumberOfsline){
						fseek(fpsourceFile, 0, SEEK_SET );
					 	istatus= readFile(fpsourceFile,--icurrentsline);
				 	}else{
				 		icurrentsline=inumberOfsline;
				 		fseek(fpsourceFile, 0, SEEK_SET );
					 	istatus= readFile(fpsourceFile,icurrentsline);
				 	}

			 	break;
			 case 91:
			 	break;
			case 113:
			case 81:
				return 1;
			default:	
				break;

		}
		ckey = getch();

	}
	fclose(fpsourceFile);
	return 1;

}
/***********************************************
*	Usage function
*	The function take information about program.
*	@param int argc      number of argumans
*	@return void
*
*************************************************/
void usage(int argc){
	if(argc == 1){

		printf("Must enter file name and number of sline.\n");
		printf("Command format is:\n");
		printf("./moreOrLess <sfileName> <inumberOfsline>\n");
		printf("Program is closing\n");
	} 
	else if(argc == 2){
		printf("Must enter number of sline\n");
		printf("Command format is:\n");
		printf("./moreOrLess <sfileName> <inumberOfsline>\n");
		printf("Program is closing\n");

	}
	else if(argc > 3){
		printf("You entered more argument\n");
		printf("Command format is:\n");
		printf("./moreOrLess <sfileName> <inumberOfsline>\n");
		printf("Program is closing\n");

	}


}
/**********************************************
*			Read File
*	The function help reading from target file.
*	@param  FILE* point the file
*	@param int take number of sline
*	@return int success
**********************************************/
int readFile(FILE * fpsourceFile , int inumberOfsline){

	char *sline;
	int icounter = 0;
	sline = (char *) malloc( sizeof(char) * sbufferSIZE);
	while(icounter < inumberOfsline){
		fgets(sline,sbufferSIZE,fpsourceFile);
		if(feof(fpsourceFile)){
			free(sline);
			return -1;
		}
		fprintf(stdout,"%s",sline);
		icounter++;

	}
	
	free(sline);
	return 0;
}

/*
	Enter the ckey without ENTER charachter.
	I taked it on website.
	http://stackoverflow.com/questions/7469139/what-is-equivalent-to-getch-getche-in-linux

*/
char getch() {
	char buf = 0;
	struct termios old = {'\n'};
	if (tcgetattr(0, &old) < 0)
		      perror("tcsetattr()");
	old.c_lflag &= ~ICANON;
	old.c_lflag &= ~ECHO;
	old.c_cc[VMIN] = 1;
	old.c_cc[VTIME] = 0;
	if (tcsetattr(0, TCSANOW, &old) < 0)
		      perror("tcsetattr ICANON");
	if (read(0, &buf, 1) < 0)
		      perror ("read()");
	old.c_lflag |= ICANON;
	old.c_lflag |= ECHO;
	if (tcsetattr(0, TCSADRAIN, &old) < 0)
		      perror ("tcsetattr ~ICANON");
	return (buf);
}
