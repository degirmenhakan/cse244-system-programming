#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <signal.h>
#include <semaphore.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

#define PERMISSION O_CREAT | O_RDWR, S_IRWXU | S_IRWXG
#define PERMISSIONSEM O_CREAT, S_IRUSR | S_IWUSR
#define SHMOBJ_PATH         "/shmMine"
#define MILLION 1000000L
#define ARRAYSIZE 5000
#define BUFFERSIZE 255
#define TRUE 1
#define FALSE -1

sem_t * sem_id;
sem_t * sem_id2;




char directoryList[ARRAYSIZE][BUFFERSIZE];
char fileList[ARRAYSIZE][BUFFERSIZE];
int direcListSize=0;
int fileListSize=0;


struct  shared_data{
	pid_t pid;
	int value;

};

struct directory{
	char direcArr[ARRAYSIZE][BUFFERSIZE];
	int size;
};

struct word{
	char word[BUFFERSIZE];
	int size;
	int point;
};

struct wordArr_t{
	struct word words[ARRAYSIZE];
	int size;


};

struct word wArray[ARRAYSIZE];
int wArraySize=0;
pthread_t *thread;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
char sharedArrayClient[255];
void sortArray(struct directory path);
int isdirectory(char *path);

void * search(void * fileName);
void * traceFile(void * fName);
int isContain(char *word);
int insertWordArray(char *word);
int isValid(char *word);
FILE *logFile;
char logFileName[BUFFERSIZE];
void ctrlC(int signum)
{
		/**
		*	Time
		**/
		time_t t = time(NULL);
		struct tm tm = *localtime(&t);

		printf("Ctrl^C ile sonlandirildi.\n");

        /**
         * Semaphore unlink: Remove a named semaphore  from the system.
         */
        if ( shm_unlink("/mysem") < 0 )
        {
        	perror("shm_unlink");
        }
        /**
         * Semaphore Close: Close a named semaphore
         */
        if ( sem_close(sem_id) < 0 )
        {
        	perror("sem_close");
        }

        /**
         * Semaphore unlink: Remove a named semaphore  from the system.
         */
       
        if ( sem_close(sem_id2) < 0 )
        {
        	perror("sem_close2");
        }
      
		logFile=fopen(logFileName,"a");
	    if(logFile==NULL){
	    	fprintf(stderr,"Cannot open File: %s\n",logFileName);
	    	exit(1);
	    }
	    fprintf(logFile,"Ctrl^c ile sonlandirildi.\n");
	    fprintf(logFile,"	      %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

   // Terminate program
   exit(signum);
}

int main(int argc, char ** argv){


	int value;
	int i=0;
	int shm_size=(1*sizeof( struct shared_data));
	int shmfd,shmfdArray,shmfdDirec;
	
	struct shared_data *shared_msg;


	struct directory *shm_array;

	struct directory *shm_direcArray;
	char directToServer[255];


	/*
		FOR MAIN FIFO	
	*/
	char connect[BUFFERSIZE];
	int fd_w;


	
	/**
	*	For word array shared
	**/
	struct wordArr_t *wA;
	char sharedWordArrayName[BUFFERSIZE];
	int shared_fd_word_array;
	//struct shared_data *shared_msg;
	/**
	*	Time
	**/
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	/**
	*	signal
	**/
	struct sigaction ctrlc;
	// time
	long timedif1;
	struct timeval tpend1;
	struct timeval tpstart1;

	ctrlc.sa_handler = ctrlC;
    ctrlc.sa_flags = 0;
    if (gettimeofday(&tpstart1, NULL)) {
		fprintf(stderr, "Failed to get start time\n");
		exit(-1);
	}
	if((sigemptyset(&ctrlc.sa_mask) || sigaction(SIGINT, &ctrlc, NULL)) == -1){
		perror("Couldn't set signal handler for SIGINT");
		return 1;
	}
	
	if(argc!=2){
		printf("Usaage: \n");
		return 0;
	
	}
	else{

		sprintf(logFileName,"Report%d.log",getpid());
		//wArray=(struct word *)malloc(sizeof(struct word)*ARRAYSIZE);
		value=atoi(argv[1]);
		

		/**
		 * Semaphore open
		 **/
		
		sem_id=sem_open("/mysem", PERMISSIONSEM, 1);
		sem_id2=sem_open("/mysem2", PERMISSIONSEM, 1);



		/**
		*	For Connection Server
		**/
		
		
	
		sprintf(connect,"%d %d ",getpid(),value);
	    printf("Client %d\n",getpid());
	    if((fd_w = open("connection", O_WRONLY)) == -1){
			fprintf(stderr,"Server is not starting\n");
			return FALSE;
		}
		write(fd_w,connect,BUFFERSIZE);
		close(fd_w);
	    //sem_post(sem_id);
	    sleep(1);

	    logFile=fopen(logFileName,"a");
	    if(logFile==NULL){
	    	fprintf(stderr,"Cannot open File: %s\n",logFileName);
	    	return 1;
	    }
	    fprintf(logFile,"---------------------Connected to Server---------------------------\n");
		fprintf(logFile,"	      %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
		fprintf(logFile,"_________________________________________________________________________\n");
		fprintf(logFile,"İstenilen path sayısı: %d\n",value);
		fclose(logFile);
		/**
		*	mutex lock init
		**/
		if(pthread_mutex_init(&lock,NULL) !=0){
			perror("Mutex init failed");
			return FALSE;

		}
	    /**
	    *	For Take Array
	    **/
	    
	
	    sprintf(sharedArrayClient,"/shmMineArray%d",getpid());
	    shmfdArray= shm_open(sharedArrayClient,PERMISSION);
	    if(shmfdArray<0 ){

			fprintf(stderr,"Shared memory cannot create.\n");
			return 0;
		}
		//printf("%s\n",sharedArrayClient);
		
		ftruncate(shmfdArray,sizeof(struct directory));


	    shm_array= (struct directory *)mmap(NULL, sizeof(struct directory), PROT_READ | PROT_WRITE, MAP_SHARED, shmfdArray, 0);
	    logFile=fopen(logFileName,"a");
	    if(logFile==NULL){
	    	fprintf(stderr,"Cannot open File: %s\n",logFileName);
	    	return 1;
	    }
	    fprintf(logFile,"Alınan dosya isimleri: \n");
	    for(i=0;i<shm_array->size;++i)
	    	fprintf(logFile," %s\n",shm_array->direcArr[i]);
	    fprintf(logFile,"Alınan dosya isimleri sayısı: %d\n",shm_array->size);
	  	fprintf(logFile,"_________________________________________________________________________\n");
	   
	   	fclose(logFile);
	   

	    /********************
		*	Send Directories
		* ******************/

 		sortArray(*shm_array);
	    sprintf(directToServer,"%sA",sharedArrayClient);
	   // printf("----------->>>>> %s\n",directToServer);
		shmfdDirec= shm_open(directToServer,PERMISSION);
	    if(shmfdDirec<0 ){

			fprintf(stderr,"Shared memory cannot create.\n");
			return 0;
		}

		ftruncate(shmfdDirec,sizeof(struct directory));
		logFile=fopen(logFileName,"a");
	    if(logFile==NULL){
	    	fprintf(stderr,"Cannot open File: %s\n",logFileName);
	    	return 1;
	    }
		fprintf(logFile,"Servera geri gönderilen klasor isimleri: \n");
	    shm_direcArray= (struct directory *)mmap(NULL, sizeof(struct directory), PROT_READ | PROT_WRITE, MAP_SHARED, shmfdDirec, 0);
	    
	    shm_direcArray->size=0;
	    for(i=0;i<direcListSize;++i){
	    	strcpy(shm_direcArray->direcArr[i],directoryList[i]);
	    	//printf("%s\n",shm_direcArray->direcArr[i]);
	    	shm_direcArray->size++;
	    }
	
	    fprintf(logFile,"Servera geri gönderilen klasor isimleri sayısı: %d\n",direcListSize);
	    fprintf(logFile,"_________________________________________________________________________\n");
	    fclose(logFile);
	    thread=(pthread_t *)malloc(sizeof(pthread_t)*fileListSize);

	   for(i=0;i<fileListSize;++i)
	    	pthread_create(&thread[i],NULL,traceFile,(void *)fileList[i]);

	    
	    for(i=0;i<fileListSize;++i)
	    	pthread_join(thread[i],NULL);
	 //   free(thread);
	    //for(i=0;i<fileListSize;++i)
	   		//traceFile((void *)fileList[i]);
	    pthread_mutex_destroy(&lock);

	    printf("_________________WORDS________________\n");

	    for(i=0;i<wArraySize;++i)
	    	printf("%s 	%d\n",wArray[i].word,wArray[i].size);

		
	    /**********************************
	    *	word Array send
	    ***********************************/
	    sprintf(sharedWordArrayName,"/shm_w_%d",getpid());
	    shared_fd_word_array = shm_open(sharedWordArrayName,PERMISSION);
	    if(shared_fd_word_array<0 ){

			fprintf(stderr,"Shared memory cannot create.\n");
			return 0;
		}

		ftruncate(shared_fd_word_array,sizeof(struct wordArr_t));

		
	    wA= (struct wordArr_t *)mmap(NULL, sizeof(struct wordArr_t), PROT_READ | PROT_WRITE, MAP_SHARED, shared_fd_word_array, 0);

	    for(i=0;i<wArraySize;++i){
	    	//printf("%s 	%d\n",wArray[i].word,wArray[i].size);
	    	strcpy(wA->words[i].word,wArray[i].word);
	    	wA->words[i].size = wArray[i].size;
	    	wA->words[i].point = 0;

	    }
	    wA->size= wArraySize;
	    
	    /*if (shm_unlink(sharedWordArrayName) != 0) {       
     		perror("In shm_unlink()");
		}
		
		*/

		sleep(2);
		logFile=fopen(logFileName,"a");
	    if(logFile==NULL){
	    	fprintf(stderr,"Cannot open File: %s\n",logFileName);
	    	return 1;
	    }
		fprintf(logFile,"_________________________________________________________________________\n");
		fprintf(logFile,"Kelime\t\tAdet\t\tPuan\n");
		fprintf(logFile,"--------------------------------------------------------------------------\n");
		for(i=0;i<wArraySize;++i)
			fprintf(logFile,"%s\t\t%d\t\t%d\n",wA->words[i].word,wA->words[i].size,wA->words[i].point);
		fprintf(logFile,"_________________________________________________________________________\n");
		
		if (gettimeofday(&tpend1, NULL)) {
			fprintf(stderr, "Failed to get end time\n");
			exit(-1);
		}
		timedif1 = MILLION*(tpend1.tv_sec - tpstart1.tv_sec) +tpend1.tv_usec - tpstart1.tv_usec;
		fprintf(logFile,"Madenci %ld süre calistı.\n",timedif1);
		fclose(logFile);
	    if (shm_unlink(sharedArrayClient) != 0) {
		        perror("In shm_unlink()");
		}
	
	}

	

	return TRUE;

}
void * traceFile(void * fName) {

	FILE *file;
	char word[255];
	char *line;
	int offset;
	int totalOffset=0;
	int i=0;
	int status;
	
	char fileName[255];
	// time
	long timedif;
	struct timeval tpend;
	struct timeval tpstart;
	//mutexlock
	pthread_mutex_lock(&lock);
	logFile=fopen(logFileName,"a");
	if(logFile==NULL){
		 	fprintf(stderr,"Cannot open File: %s\n",logFileName);
	    	exit(1);
	 }
	fprintf(logFile,"_________________________________________________________________________\n");
	fprintf(logFile,"Thread: %ld  File: %s\n",pthread_self(),(char *)fName);

	if (gettimeofday(&tpstart, NULL)) {
		fprintf(stderr, "Failed to get start time\n");
		exit(-1);
	}
	strcpy(fileName,(char *)fName);

	/*Verilen dosyayi acip kelimeleri ayirip gecerli olanlari verilen fifoya yazar*/
	if((file=fopen(fileName,"r"))==NULL){
		fprintf(stderr,"Dosya acilamadi.%s\n",fileName);
		exit (EXIT_FAILURE);

	}
	i=0;
	line =(char *)malloc(sizeof(char)*255);
	while(!feof(file) && fgets(line,255,file) != NULL){
		while(sscanf(line,"%s%n",word,&offset)==1){
			
			//printf("%d %s %s\n",i,line,fileName);
			status=insertWordArray(word);
			if(status==FALSE)
			{
				fprintf(stderr,"Word array is full\n");
				exit(FALSE);
			}
	
			line+=offset;
			i++;
		}
	}
	fclose(file);
	
	

	//mutex unlock
	if (gettimeofday(&tpend, NULL)) {
		fprintf(stderr, "Failed to get end time\n");
		exit(-1);
	}

	timedif = MILLION*(tpend.tv_sec - tpstart.tv_sec) +tpend.tv_usec - tpstart.tv_usec;
	logFile=fopen(logFileName,"a");
	if(logFile==NULL){
		 	fprintf(stderr,"Cannot open File: %s\n",logFileName);
	    	exit(1);
	 }
	fprintf(logFile,"Tid: %ld Dosya adi : %s Gecen Sure: %ld \n",pthread_self(),fileName,timedif);	
	fclose(logFile);
	pthread_mutex_unlock(&lock);
	
	return (void*)1;
	
}
void sortArray(struct directory path){
	int i=0;
	for(i=0;i<path.size;++i){
		if(isdirectory(path.direcArr[i])){
			strcpy(directoryList[direcListSize],path.direcArr[i]);
			//printf("Directory: %s\n",directoryList[direcListSize]);
			direcListSize++;
		}else{
			strcpy(fileList[fileListSize],path.direcArr[i]);
			//printf("File: %s\n",fileList[fileListSize]);
			fileListSize++;

		}


	}


}
int isdirectory(char *path) {
   struct stat statbuf;

   if (stat(path, &statbuf) == FALSE)
      return 0;
   else
      return S_ISDIR(statbuf.st_mode);
}

int isContain(char *word){

	int i=0;

	for(i=0;i<wArraySize;++i){
		if(strcmp(wArray[i].word,word)==0)
			return i;

	}
	return FALSE;

}

int insertWordArray(char *word){
	int index=0;
	index=isContain(word);

	
	if(isValid(word)==TRUE){
		if(index==FALSE){
			if(wArraySize>=ARRAYSIZE)
				return FALSE;
			strcpy(wArray[wArraySize].word,word);
			wArray[wArraySize].point=0;
			wArray[wArraySize++].size++;
			return TRUE;
		}else{
			wArray[index].size++;
			wArray[index].point=0;
			return TRUE;
		}
	}
	return TRUE;
	
}
int isValid(char *word){
	int i=0;
	int status=TRUE;

	for(i=0;i<strlen(word);++i){
		if(word[i]>=65 && word[i]<=122){
			if(word[i]>=91 && word[i]<=96)
				return FALSE;
		}else{
			return FALSE;
		}

	}
	return TRUE;
}