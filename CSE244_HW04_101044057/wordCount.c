/**
*	@author Hakan DEĞİRMEN Num:101044057
*	@version v.1.0
*	@since 22.04.2015
*/

/**
*	         COMPILER & RUN
*		_________________________
*	
*	*	gcc -c wordCount.c
*  	*	gcc wordCount.o -o wordCount
*	*	./wordCount <directoryName>
*
***/


/****************************************************
*					KUTUPHANELER
*****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <time.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#define MILLION 1000000L



/**
*	Bu fonksiyon verilen pathdeki
*	tüm klasor ve dosyaları bulma
*	ya yarar.
*
	*@param string Path
**/
void dizinDolas(char *strDirName);

/**
*		Verilen pathin klasor olup olmadigini dönderir
*		@param string Path
*		@return int isDirectory path klasorse modunu degilse 0 donderir.
**/
int isdirectory(char *path);

/**
*	Thread fonksiyonu Verile file adında kelime sayacak olan fonksiyondur.
*	@param fileName kelime sayilacak dosyanin adi
*	@param void * o file da kac tane gecerli kelime varsa onu donderecek
*	
**/
void* search(void * voidDirName);





/**
*			GLOBAL VARIABLES
**/

int totalWord=0;
int totalFile=0;
int totalChild=0;
int totalDirectory=0;
int intFileUsed=0;
char strFilePaths[1024][255];



/**
*	Main Function Driver function
*	
*	@param int argc number of arguments   
*	@param char** argv  arguments
*	@return success
**/

int main(int argc, char ** argv) {
	char strDirName[255];
	int iLineNumber=1,i;
	char strDir[255];
	char secim;
	void* counter;
	char strLine[255];
	FILE *resultFile;
	pthread_t threads[180];
	//Time
	long timedif;
	struct timeval tpend;
	struct timeval tpstart;

	if(argc != 2){
		printf("Usage:./wordCount <directoryName>\nProgram is closing...");
		return 0;
	}else{

		if (gettimeofday(&tpstart, NULL)) {
			fprintf(stderr, "Failed to get start time\n");
			exit(-1);
		}

		strcpy(strDirName,argv[1]);/*Argumanlari her ihtimale karsi kopyaliyorum*/
	
		getcwd(strDir,255);		//get current working directory
		strcat(strDir,"/");
		strcat(strDir,argv[1]);//yeni directoryi ayarliyorum
		if(!isdirectory(strDir)){//directory yoksa kapatiliyor
			printf("Böle bir directory yok program kapatiliyor.\n");
			return 0;
		}
		dizinDolas(strDir);//duzgun ise recursive bi sekilde icindekileri listeyeyecek
	
		for(i=0;i<intFileUsed;++i){
			pthread_create(&threads[i],NULL,search,(void *)strFilePaths[i]);
		}
		for(i=0;i<intFileUsed;++i){
			pthread_join(threads[i],&counter);
			totalWord+=(int)counter;
		}

		resultFile=fopen("result.log","a");
		fprintf(resultFile,"_____________________________________________________\n");
		fprintf(resultFile,"Total Directory:%d | Total Thread:%d | Total Word: %d | Total File: %d\n",totalDirectory,intFileUsed,totalWord,intFileUsed);
		fclose(resultFile);
		if (gettimeofday(&tpend, NULL)) {
			fprintf(stderr, "Failed to get end time\n");
			exit(-1);
		}

		timedif = MILLION*(tpend.tv_sec - tpstart.tv_sec) +tpend.tv_usec - tpstart.tv_usec;
		fprintf(stdout,"Total Directory:%d | Total Thread:%d | Total Word: %d | Total File: %d | Total microsecond: %ld\n",totalDirectory,intFileUsed,totalWord,intFileUsed,timedif);
		
	}
		
		
	return 0;
}/*main End*/


/**
*	Bu fonksiyon verilen pathdeki
*	tüm klasor ve dosyaları bulma
*	ya yarar.
*
*	@param string Path
**/

void dizinDolas(char *strDirName){

		struct dirent *direntp;
    	DIR    *dirp;
    	char   *strNewPath;
		char    *strFilePath;
		int 	iDirSize;
		char *strRoot;
		/*gerekli yerleri kopyaliyorum*/
		strNewPath=(char *)malloc(sizeof(char)*255);
    	strFilePath=(char *)malloc(sizeof(char)*255);
		strRoot=(char *)malloc(sizeof(char)*255);

		strcat(strNewPath,strDirName);
		/*directoryi aciyorum acilamazsa hata veriyorum*/
		
		if ((dirp = opendir(strDirName)) == NULL) {
				fprintf (stderr, "Failed to open directory \"%s\" : %s\n",
				         direntp->d_name, strerror(errno));
				exit(0);
		}
		while((direntp=readdir(dirp))!=NULL){//directoryde eleman kalmayana kadar okuma yapıyorum

			if(direntp->d_type & DT_DIR)//klosorse
			{
				
					iDirSize=strlen(direntp->d_name);/*dosya ad uzunlugunu alip .. veya . olmamasına dikkat ediyorum yoksa beni sürekli geri atar*/
					
					
					if(iDirSize==2 && direntp->d_name[0]=='.' && direntp->d_name[1]=='.'){
							
					}else if(iDirSize==1 && direntp->d_name[0]=='.' ){
						
					}else
					{	
						totalDirectory++;
						strcpy(strRoot,strNewPath);/*yeni klasorle beraber yeni pathi beirliyorum ve recursive bi sekilde bitene kadar yapiyorum*/
						strcat(strRoot,"/");
						strcat(strRoot,direntp->d_name);
						
						if(strlen(strRoot)<=255)/*Unix path uzunlugunu 255 den fazla izin vermedigi icin kontrol ediyorum*/
							dizinDolas(strRoot);	
						else{
							printf("Dosya isim boyutu asti\nProgram kapatiliyor.\n");
							break;
						}
				
					}
						
					

					
			}
			else{//dosya ise
				
				iDirSize=strlen(direntp->d_name);

			
				if(direntp->d_name[iDirSize-1] != '~'){
					strcpy(strFilePath,strDirName);/*Yeni dosya ile beraber yeni path buluyorum*/
					strcat(strFilePath,"/");
					strcat(strFilePath,direntp->d_name);
					if(strlen(strFilePath)<=255){//arama fonksiyonunu gonderiyorum
						
						strcpy(strFilePaths[intFileUsed++],strFilePath);
						//search((void *)strFilePath);
					}
					else{
						printf("Dosya isim boyutu asti\nProgram kapatiliyor.\n");
						break;
					}
					
				}
				

			}
			
			
		}
		free(strNewPath);//mallocla aldıgım yerleri geri veriyorum
		free(strFilePath);
		free(strRoot);
		if (closedir (dirp)) {//dosyayı kapatiyorum
			fprintf (stderr, "'%s:	dosyasi kapatilamadi %s\n",strDirName, strerror (errno));
	   		 exit (EXIT_FAILURE);		
 		}
		
	
}/*DizinDolas end*/


/**
*	Verile file adında kelime sayacak olan fonksiyondur.
*	@param fileName kelime sayilacak dosyanin adi
*	
**/
void *search(void * voidDirName){
	FILE* finput;
	char strLine[4096];
	int iSatir=0;
	int totalWord=0;
	int lenght=0;
	int counter=0;
	char word[255];
	int ofset;
	int i=0;
	int status=0;
	char *tempLine;
	char *strDirName=(char *)voidDirName;
	int totalOffset=0;
	//Time
	long timedif;
	struct timeval tpend;
	struct timeval tpstart;



	FILE *resultFile;//sonuclarımı yazacagım dosya

	//strLine=(char *)malloc(sizeof(char)*1024);
	if((finput=fopen(strDirName,"r"))==NULL){
		printf("%s dosyasi acilamadi\n",strDirName);
	}else{
		totalFile++;
	
		resultFile=fopen("result.log","a");
		if (gettimeofday(&tpstart, NULL)) {
			fprintf(stderr, "Failed to get start time\n");
			exit(-1);
		}
		
		while(!feof(finput) && fgets(strLine,4096,finput) != NULL){
				//tempLine=(char *)malloc(sizeof(char)*strlen(strLine));
				//strcpy(tempLine,strLine);
			tempLine=strLine;
				//wordControl
				while(sscanf(tempLine,"%s%n",word,&ofset)==1){

					for(i=0;i<strlen(word);++i){
						if(word[i]>=65 && word[i]<=122){
							if(word[i]>=91 && word[i]<=96)
								status=-1;
						}else{
							status=-1;
						}

					}
					if(status!=-1)
						counter++;
					status=0;
					tempLine+=ofset;
					totalOffset+=ofset;
				}
				status=0;

				//tempLine-=totalOffset;
				//free(tempLine);
				
				iSatir++;
				
		}
		if (gettimeofday(&tpend, NULL)) {
			fprintf(stderr, "Failed to get end time\n");
			exit(-1);
		}

		timedif = MILLION*(tpend.tv_sec - tpstart.tv_sec) +tpend.tv_usec - tpstart.tv_usec;
		fprintf(resultFile,"_____________________________________________________\n");
		fprintf(resultFile,"ThreadId:%ld\nFileName: %s\nWord Count: %d\nTime:%ld microsecond\n",pthread_self(),strDirName,counter,timedif);

		
		
		fclose(resultFile);/*dosya kapama*/
	
		
		//free(strLine);	
		fclose(finput);/*dosya kapama*/
		return (void*)counter;


	}

	




}/*search end*/


/**
*	Verilen bir satirda kac tane gecerli kelime var onu sayacal
*	@param string line  source line
*	@return int count of valid word
**/
int searchLine(char chSatir[]) {
	int lenght=0;
	int counter=0;
	char word[255];
	int ofset;
	int i=0;
	int status=0;
	char *tempLine= (char *)malloc(sizeof(char)*strlen(chSatir));
	strcpy(tempLine,chSatir);

	while(sscanf(tempLine,"%s%n",word,&ofset)==1){
		
		for(i=0;i<strlen(word);++i){
			if(word[i]>=65 && word[i]<=122){
				if(word[i]>=91 && word[i]<=96)
					status=-1;
			}else{
				status=-1;
			}

		}
		if(status!=-1)
			counter++;
		status=0;

		tempLine+=ofset;
		
	}
	return counter;
}/*search fonksiyon sonu*/

int isdirectory(char *path) {
   struct stat statbuf;

   if (stat(path, &statbuf) == -1)
      return 0;
   else
      return S_ISDIR(statbuf.st_mode);
}



void result(){
	FILE *sourceFile;
	char word[255];
	int temp;
	int num;

	if((sourceFile=fopen("result.txt","r"))==NULL){
		printf("Cannot open result file\n");

	}
	while(!feof(sourceFile)){
		fscanf(sourceFile,"%s",word);
		if(strcmp(word,"TotalWordIs")==0){
			fscanf(sourceFile,"%d",&num);
			printf("num: %d\n",num);
			totalWord+=num;
		}

	}
	printf("Total directory:	%d\n",totalDirectory);
	printf("Total file:		%d\n",intFileUsed);
	printf("Total word:		%d\n",totalWord);
	
	fclose(sourceFile);

}