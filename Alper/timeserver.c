/************************************************************
                      LIBRARIES
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>
#include <math.h>
#include <float.h>



/************************************************************
                          MACROS
************************************************************/
#define SUCCESS  1
#define UNSUCCESS  -1
#define TRUE  1
#define FALSE  -1
#define BUFFERSIZE  255
#define LITTLEBUFFER  20
#define TYPENUM  5
#define DEFAULT_INT_VALUE  0
#define PARAM_SIZE  4
#define PERMISSION 	0777
#define PROCESSSIZE 500
#define MATRIX_MAX_SIZE 20
#define MAX_BUFFER_SIZE 4096

/***********************************************************
                        STRUCTURE
************************************************************/
struct type_client_request {
   int pid;
   int request_count;
   int matrix_size;
   float matrix[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE];
};



/************************************************************
                      FUNCTION PROTOTYPES
************************************************************/
void usage();
void get_params(char **argv);
int start_server();
int listen_to_request();
//void generate_matrix(int matrix[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE],int matrix_size);
/**
*	Pipedan okuma fonksiyonu
*	@param file nerden okuyacagi
*	@param buffer nereye okuyacagi
**/
void read_from_pipe (int file,char buffer[BUFFERSIZE]);
/**
*	Pipe a yazma fonksiyonu
*	@param file nereye yazacagi
*	@param buffer neyi yazacagi
**/
void write_to_pipe (int file,char message[BUFFERSIZE]);

void parse_client_data(int *process_id, int *request_count, char buffer[BUFFERSIZE]);

void print_clients_data(struct type_client_request client_data);

void clear_clients_data(struct type_client_request client_data);

float determinant_function(float coefficient[][MATRIX_MAX_SIZE],int size);

int swap(float matrix[][MATRIX_MAX_SIZE],int row,int size);

void copy(float matrix1[][MATRIX_MAX_SIZE],float matrix2[][MATRIX_MAX_SIZE],int size);

void matrix_to_string(float matrix[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE], int matrix_size, char buffer[MAX_BUFFER_SIZE]);

void generate_matrix(float matrix[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE],int matrix_size);

int is_inversible_matrix(float matrix[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE],int matrix_size);

void print_matrix (float result_matrix[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE], int matrix_size);


/************************************************************
                      GLOBAL VARIABLES
************************************************************/



/**
*	CTRL^C islemi icin gerekli yedekleme icin tutuyorum
**/
pid_t childProcess;
int childProcesses[PROCESSSIZE];
int clientPid[PROCESSSIZE];
int tick_time = DEFAULT_INT_VALUE;
int matrix_size = DEFAULT_INT_VALUE;
char mainPipeName[BUFFERSIZE];
struct type_client_request clients[PROCESSSIZE];

/*Other*/
int clientNumber=0;


int main(int argc, char *argv[]){

  if(argc != PARAM_SIZE){
    usage();
    return UNSUCCESS;
  }

  get_params(argv);

  start_server();

}

int start_server(){

    char fifoName[BUFFERSIZE];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    struct itimerval it_val;  /* for setting itimer */

    strcpy(fifoName, mainPipeName);
    if (mkfifo(fifoName,PERMISSION) == -1) {

        perror("failed mkfifo\n");
            unlink(fifoName);
            return UNSUCCESS;
    }
    printf("		SERVER STARTED		\n");
    printf("	      %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    printf("__________________________________________________\n");

    if (signal(SIGALRM, (void (*)(int)) listen_to_request) == SIG_ERR) {
        perror("Unable to catch SIGALRM");
        exit(1);
    }

    it_val.it_value.tv_sec =     tick_time/1000;
    it_val.it_value.tv_usec =    (tick_time*1000) % 1000000;
    it_val.it_interval = it_val.it_value;
    if (setitimer(ITIMER_REAL, &it_val, NULL) == -1) {
        perror("error calling setitimer()");
        exit(1);
    }

    while(1)
        pause();

}

int listen_to_request(){

    int  fd,fd_r,fd_w,i;
    int	 readValue=0;
    char buffer[BUFFERSIZE];


    int ms_fd_r;
    int ms_fd_w;

    //For pipe
    int fdPipe[PROCESSSIZE][2];
    char pipeBuffer[BUFFERSIZE];
    int nbytes=0;


    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    //For parameters
    int waitTime;
    int paramNumber;
    double paramValues[12];
    char *token;
    int k=0, j=0;
    char fifo_name[BUFFERSIZE];
    char fifo_name_main_client[BUFFERSIZE];
    int request_count = 0;
    struct type_client_request client_data;
    char bigest_buffer[MAX_BUFFER_SIZE] ;
    srand(time(NULL));   // should only be called once

    sprintf(bigest_buffer, "\0");
    strcpy(fifo_name, mainPipeName);
    printf("gelen giden yok\n");

    if((fd_r = open(fifo_name, O_RDONLY)) == -1){
      fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",fifo_name);
      return UNSUCCESS;
    }
    readValue=read(fd_r,buffer,BUFFERSIZE);
    if (readValue>0){
        printf("Read data: %s  \n",buffer);
        close(fd_r);

        clientPid[clientNumber]=atoi(strtok(buffer," "));
        printf("---------->%s\n",buffer);
        t=time(NULL);
        tm = *localtime(&t);

        printf("%d.Client connected.\n",clientNumber+1);
        clientNumber++;

        ///* Create the pipe.
        if (pipe (fdPipe[clientNumber-1]))
        {
            fprintf (stderr, "Pipe failed.\n");
            return EXIT_FAILURE;
        }

        childProcess = fork();

        if(childProcess==-1){

            fprintf ( stderr ,"Cannot create new process.\nProgram is closing.\n");
            return UNSUCCESS;

        }else if(childProcess==0){


            printf("Server pid is: %d\n",getpid());
            printf("Client pid is: %s\n",buffer);
            client_data.matrix_size = matrix_size;
            clear_clients_data(client_data);
            parse_client_data(&client_data.pid, &client_data.request_count, buffer);
            printf("Test: %d %d\n",waitTime, readValue);
            do{

                generate_matrix(client_data.matrix, matrix_size);
            }while(is_inversible_matrix(client_data.matrix, matrix_size) == FALSE);

            print_clients_data(client_data);

            sprintf(fifo_name_main_client,"%s_%d_%d",fifo_name, client_data.pid, client_data.request_count);
            printf("kücük client name: ---> %s\n",fifo_name_main_client);

            ///*Pipe cominication
            close(fdPipe[clientNumber-1][0]);
            write_to_pipe (fdPipe[clientNumber-1][1],fifo_name_main_client);


            if (mkfifo(fifo_name_main_client,PERMISSION) == -1) {
                perror("failed mkfifo\n");
                unlink(fifo_name_main_client);
                return UNSUCCESS;
            }

            if((ms_fd_w = open(fifo_name_main_client, O_WRONLY)) == -1){
                fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",fifo_name_main_client);
                return UNSUCCESS;
            }

            //send matrix
            strcpy(buffer,"\0");
            sprintf(buffer,"%d ",client_data.matrix_size);
            write(ms_fd_w, buffer,BUFFERSIZE);
            close(ms_fd_w);

            sleep(1);


            if((ms_fd_w = open(fifo_name_main_client, O_WRONLY)) == -1){
                fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",fifo_name_main_client);
                return UNSUCCESS;
            }
            matrix_to_string(client_data.matrix, client_data.matrix_size, bigest_buffer);
            write(ms_fd_w,bigest_buffer,MAX_BUFFER_SIZE);
            close(ms_fd_w);
            printf("Sending data Finished\n");


        }else{

            close(fdPipe[clientNumber-1][1]);

            //read_from_pipe (fdPipe[clientNumber-1][0],pipeBuffer);

            /*pipe ile alinan fifo adi  mainFifo ile  cliente verildi
            if((fd_w = open(fifo_name,O_WRONLY))==-1){
                fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",fifo_name);

                return UNSUCCESS;
            }
            strcpy(buffer,"\0");
            ///*send fifoname
            sprintf(buffer,"%d %s",clientPid[clientNumber-1],pipeBuffer);
            write(fd_w,buffer,BUFFERSIZE);
            close(fd_w);*/

        }
    }
}

void get_params(char **argv){
  tick_time = atoi(argv[1]);
  matrix_size = 2 * atoi(argv[2]);
  strcpy(mainPipeName, argv[3]);
}


void usage(){
  printf("__________________________________________________________________________________\n\n");
  printf("\t\t\t\t\tUSAGE\t\t\t\t\t\n");
  printf("\t\t\t\t------------------\t\t\t\n");
  printf("\t./timerserver <ticks in milsec> <n> <mainPipeName>\n");
  printf("\tticks in milisec is integer value\n");
  printf("\tn is integer value.Size of matrix\n");
  printf("\tmainPipeName is string. The name of file is that comminication between process\n");
  printf("__________________________________________________________________________________\n\n");
}

void read_from_pipe (int file,char buffer[BUFFERSIZE])
{
    FILE *stream;
    int c,i=0;
    stream = fdopen (file, "r");
    while ((c = fgetc (stream)) != EOF){
        buffer[i]=c;
        i++;
    }
    buffer[i]='\0';
    fclose (stream);
}
void write_to_pipe (int file,char message[BUFFERSIZE])
{
    FILE *stream;

    stream = fdopen (file, "w");
    fprintf(stream,"%s",message);
    fclose (stream);
}

void parse_client_data(int *process_id, int *request_count, char buffer[BUFFERSIZE]){
    const char s[2] = "_";
    char *token;

    token = strtok(buffer, s);

    *process_id = atoi(token);

    token = strtok(NULL, s);

    *request_count = atoi(token);


}

void print_clients_data(struct type_client_request client_data){
    int i = 0, j=0;
    printf("-----------------------------------------------\n");
    printf("\t\t\tClient process id is: %d\n",client_data.pid);
    printf("\t\t\tClient request number is: %d\n",client_data.request_count);
    printf("\t\t\tClient matrix size is: %d\n",client_data.matrix_size);
    printf("\t\t\tClient matrix is:\n");
    for(i=0 ;i< client_data.matrix_size; ++i){
        for(j=0;j<client_data.matrix_size;++j){
            printf("%.2f ",client_data.matrix[i][j]);
        }
        printf("\n");
    }
}

void clear_clients_data(struct type_client_request client_data){
    int i = 0, j=0;
    for(i=0 ;i< client_data.matrix_size; ++i){
        for(j=0;j<client_data.matrix_size;++j){
            client_data.matrix[i][j] = 1;
        }
        printf("\n");
    }

}
void generate_matrix(float matrix[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE],int matrix_size){
    int r = 0;
    int i, j;

    for(i=0 ;i< matrix_size; ++i){
        for(j=0;j<matrix_size;++j){
            matrix[i][j] = (rand() + getpid()) % 7;
        }
    }

}

void matrix_to_string(float matrix[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE], int matrix_size, char buffer[MAX_BUFFER_SIZE]){
    int i,j;
    char str[7];

    for(i=0;i<matrix_size;++i){
        for(j=0;j<matrix_size;++j){
            sprintf(str, "%.2f-",matrix[i][j]);
            strcat(buffer, str);
        }
    }
}

float determinant_function(float coefficient[][MATRIX_MAX_SIZE],int size)
{
    int i, j, k, l;
    float temp_coefficient,temp_matrix[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE];
    float determinant = 1;

    copy(coefficient,temp_matrix,size);

    for (i=0; i<size; ++i)
    {
        //printf ("%2.f\t%d\t%d\n",temp_matrix[i][i],i,j);
        if (temp_matrix[i][i] == 0)
        {
            if (swap(temp_matrix,i,size) == FALSE)
            {
                determinant = 0;
                return determinant;
            }
            else
                determinant *= -1;
        }
        if (temp_matrix[i][i] != 1)
        {
            temp_coefficient = temp_matrix[i][i];
            determinant *= temp_matrix[i][i];
            for (j=i; j<size; ++j)
                temp_matrix[i][j] = temp_matrix[i][j] / temp_coefficient;
        }
        for (k=i+1; k<size; ++k)
            for (l=(size-1); l>=i; --l)
                temp_matrix[k][l] = temp_matrix[k][l] - (temp_matrix[k][i] * temp_matrix[i][l]);
    }

    return determinant;

}

void copy(float matrix1[][MATRIX_MAX_SIZE],float matrix2[][MATRIX_MAX_SIZE],int size)
{
    int i, j;

    for (i=0; i<size; ++i)
        for (j=0; j<size; ++j)
            matrix2[i][j] = matrix1[i][j];

}

int swap(float matrix[][MATRIX_MAX_SIZE],int row,int size)
{
    int row_a = row,column_b = row, i, j;
    float temp;

    for (i=row_a; i<size; ++i)
        if (matrix[i][column_b] != 0)
        {
            for (j=column_b; j<size; ++j)
            {
                temp = matrix[row_a][j];
                matrix[row_a][j] = matrix[i][j];
                matrix[i][j] = temp;
            }
            return TRUE;
        }
        return FALSE;
}



int  is_inversible_matrix(float matrix[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE],int matrix_size){

    float result = determinant_function(matrix, matrix_size);
    printf (" Resul777t :  %.2f \n",result);
    if(result != 0 && result < FLT_MAX &&  result > FLT_MIN){
        printf("---------->>>> %.2f\n",result);
        return TRUE;
    }
    else
        return FALSE;
}

void print_matrix (float result_matrix[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE], int matrix_size){
	int i=0;
	int j=0;
	for(i=0;i<matrix_size;++i){
		for(j=0;j<matrix_size;++j){
			printf("%.2f ",result_matrix[i][j]);
		}
		printf("\n");
	}
}
