/****************************************************
*					KUTUPHANELER
*****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>



/************************************************************
                          MACROS
************************************************************/
#define SUCCESS  1
#define UNSUCCESS  -1
#define TRUE  1
#define FALSE  -1
#define BUFFERSIZE  255
#define MAX_BUFFER_SIZE 4096
#define MAX_MATRIX_SIZE 45

/************************************************************
                      FUNCTION PROTOTYPES
************************************************************/
void used();
void get_matrix(float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE] ,char str_matrix[MAX_BUFFER_SIZE], int matrix_size);
void inverse_matrix(float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE],float matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size);
void print_matrix (float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size);
void inverse_all_matrix(float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE],float matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size);
void convolution_matrix(float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE],float source[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size);
void convolution_all_matrix(float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE],float source[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size);
void clear_matrix(float source_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size,int value);

/************************************************************
                      GLOBAL VARIABLES
************************************************************/
FILE *reportFile;
char fifoName[BUFFERSIZE];
char new_request_fifo_name[BUFFERSIZE];
int serverPid;
int matrix_size = 0;


int main(int argc,char **argv){

	int fd,fd_r,fd_w;
	char buffer[BUFFERSIZE];
	char operation[BUFFERSIZE];

	int pid;
	int ms_fd_r,ms_fd_w;
	char message[BUFFERSIZE];
	int k=0;
	int i,paramNo;
    int request_count = 0;
	int childProcess, childProcess1;

	char reportFileName[BUFFERSIZE];
    char biggest_buffer[MAX_BUFFER_SIZE];
	time_t t = time(NULL);
	struct sigaction ctrlc;
	float source_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float inverse_matrix_[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float convolution_matrix_[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];


	if(argc != 2)
	{
		used();
		return 0;
	}else{
        /*
    	ctrlc.sa_handler = ctrlC;
	    ctrlc.sa_flags = 0;
	    if((sigemptyset(&ctrlc.sa_mask) || sigaction(SIGINT, &ctrlc, NULL)) == -1){
			perror("Couldn't set signal handler for SIGINT");
			return -1;
		}

		sprintf(reportFileName,"Report_%d",getpid());
		reportFile=fopen(reportFileName,"w");
        */
		strcpy(fifoName,argv[1]);
        while(1){


    		if((fd_w = open(fifoName,O_WRONLY))== -1){
    			fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",fifoName);
    			return -1;
    		}

    		sprintf(buffer,"%d_%d",getpid(), request_count);
    		write(fd_w,buffer,BUFFERSIZE);
    		close(fd_w);

            sprintf(new_request_fifo_name,"%s_%s",fifoName,buffer);

    		/***********************************
    		*	time Servera baglandı	Logladim
    		************************************/
            /*
    		fprintf(reportFile,"Connect to %s\n",fifoName);
    		struct tm tm = *localtime(&t);
    		fprintf(reportFile,"	      %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    		fprintf(reportFile,"____________________________________________________________\n");
    		sprintf(buffer,"%d %s\n",getpid(),operation);
            */
            // TODO while 1 icine alinacak
            sleep(1);
    		if((fd_r = open(new_request_fifo_name,O_RDONLY))==-1){
    			fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",new_request_fifo_name);
    			return -1;
    		}
    		while(read(fd_r,buffer,255)>0);
            matrix_size = atoi(buffer);
    		printf("matrix_size: %s\n",buffer);
    		close(fd_r);

            if((fd_r = open(new_request_fifo_name,O_RDONLY))==-1){
                fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",new_request_fifo_name);
                return -1;
            }
            while(read(fd_r,biggest_buffer,MAX_BUFFER_SIZE)>0);
            close(fd_r);
			printf("_________________________________________\n");
			//printf("%s\n",biggest_buffer);
			get_matrix(source_matrix, biggest_buffer,matrix_size);

			childProcess = fork();

	        if(childProcess==-1){

	            fprintf ( stderr ,"Cannot create new process.\nProgram is closing.\n");
	            return UNSUCCESS;

	        }else if(childProcess==0){
				printf("_______________INVERSE__________________________\n");
				inverse_all_matrix(inverse_matrix_, source_matrix, matrix_size);
				print_matrix(inverse_matrix_, matrix_size);


				childProcess1 = fork();
				if(childProcess1==-1){

		            fprintf ( stderr ,"Cannot create new process.\nProgram is closing.\n");
		            return UNSUCCESS;

		        }else if(childProcess1==0){
					printf("_______________Conv__________________________\n");
					convolution_all_matrix(convolution_matrix_, source_matrix, matrix_size);
					print_matrix(convolution_matrix_, matrix_size);

				}else{
					wait(0);
				}








			}else{
				wait(0);
			}

			print_matrix(source_matrix, matrix_size);

            /*
    		pid=atoi(strtok(buffer," "));
    		strcpy(mathServerFifoName,strtok(NULL,"\0"));

    		strcpy(message,mathServerFifoName);
    		strtok(message,"_");
    		serverPid = atoi(strtok(NULL,"_"));
    		//printf("server Pid: %d Buffer:%s  | math:%s\n",serverPid,buffer,mathServerFifoName);

    		fprintf(stderr,"mathServer's fifo name is :%s\n",mathServerFifoName);

    		if((ms_fd_w = open(mathServerFifoName,O_WRONLY))==-1){
    				fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",mathServerFifoName);
    				return -1;
    		}
    		strcpy(buffer,"\0");


    	    unlink(mathServerFifoName);
            */
            request_count ++;

    	}
    }
}

void inverse_matrix(float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE],float matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size){
    float ratio,a;
    int i, j, k;
    int n = matrix_size;

	// copy to source matrix
    for(i = 0; i < n; i++){
        for(j = 0; j < n; j++){
            result_matrix[i][j] = matrix[i][j];
        }
    }

    for(i = 0; i < n; i++){
        for(j = n; j < 2*n; j++){
            if(i==(j-n))
                result_matrix[i][j] = 1.0;
            else
                result_matrix[i][j] = 0.0;
        }
    }
    for(i = 0; i < n; i++){
        for(j = 0; j < n; j++){
            if(i!=j){
                ratio = result_matrix[j][i]/result_matrix[i][i];
                for(k = 0; k < 2*n; k++){
                    result_matrix[j][k] -= ratio * result_matrix[i][k];
                }
            }
        }
    }
    for(i = 0; i < n; i++){
        a = result_matrix[i][i];
        for(j = 0; j < 2*n; j++){
            result_matrix[i][j] /= a;
        }
    }
    for(i = 0; i < n; i++){
        for(j = n; j < 2*n; j++){
			result_matrix[i][j-n] = result_matrix[i][j];
        }
    }
}

void get_matrix(float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE] ,char str_matrix[MAX_BUFFER_SIZE], int matrix_size){
	int i=0, j=0;
	char *token;
	/* get the first token */
   token = strtok(str_matrix, "-");
   result_matrix[i][j] = atof(token);
   j ++;

   /* walk through other tokens */
   while( token != NULL )
   {
	  if(j==matrix_size)
	  {
		  j = 0;
		  i ++;
	  }
	  if(i*j == (matrix_size-1)*(matrix_size-1))
	  	break;

      token = strtok(NULL, "-");
	  result_matrix[i][j] = atof(token);
      j ++;
   }
}

void used(){
	fprintf(stderr,"Usage:\n\t./seewhat <mainPipeName>\n");
}


void print_matrix (float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size){
	int i=0;
	int j=0;
	for(i=0;i<matrix_size;++i){
		for(j=0;j<matrix_size;++j){
			printf("%.2f ",result_matrix[i][j]);
		}
		printf("\n");
	}
}

void inverse_all_matrix(float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE],float matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size){

	int i = 0;
	int j = 0;
	int k = 0;
	int t = 0;

	int half_matrix_size = matrix_size / 2;

	float result1[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float result2[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float result3[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float result4[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];

	float source1[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float source2[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float source3[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float source4[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];


	// first quarter copy
	for (i=0; i<half_matrix_size; ++i){
		for (j=0; j<half_matrix_size; ++j){
			source1[i][j] = matrix[i][j];
		}
	}

	// second quarter copy
	for (i=0; i<half_matrix_size; ++i){
		for (j=half_matrix_size; j<matrix_size; ++j){
			source2[i][j-half_matrix_size] = matrix[i][j];
		}
	}

	// third quarter copy
	for (i=half_matrix_size; i<matrix_size; ++i){
		for (j=0; j<half_matrix_size; ++j){
			source3[i-half_matrix_size][j] = matrix[i][j];
		}
	}

	// fourth quarter copy
	for (i=half_matrix_size; i<matrix_size; ++i){
		for (j=half_matrix_size; j<matrix_size; ++j){
			source4[i-half_matrix_size][j-half_matrix_size] = matrix[i][j];
		}
	}
	// calculate inverse matrix
	inverse_matrix(result1, source1, half_matrix_size);
	inverse_matrix(result2, source1, half_matrix_size);
	inverse_matrix(result3, source1, half_matrix_size);
	inverse_matrix(result4, source1, half_matrix_size);


	//merge all result4
	// first quarter copy
	for (i=0; i<half_matrix_size; ++i){
		for (j=0; j<half_matrix_size; ++j){
			result_matrix[i][j] = result1[i][j];
		}
	}

	// second quarter copy
	for (i=0; i<half_matrix_size; ++i){
		for (j=half_matrix_size; j<matrix_size; ++j){
			result_matrix[i][j] = result2[i][j - half_matrix_size];
		}
	}

	// third quarter copy
	for (i=half_matrix_size; i<matrix_size; ++i){
		for (j=0; j<half_matrix_size; ++j){
			result_matrix[i][j] = result3[i - half_matrix_size][j];
		}
	}

	// fourth quarter copy
	for (i=half_matrix_size; i<matrix_size; ++i){
		for (j=half_matrix_size; j<matrix_size; ++j){
			result_matrix[i][j] = result3[i-half_matrix_size][j- half_matrix_size];
		}
	}

}


// http://stackoverflow.com/questions/3982439/fast-2d-convolution-for-dsp adresinden aldim.
void convolution_matrix(float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE],float source[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size){
	// find center position of kernel (half of kernel size)
	int kCenterX, kCenterY;
	int kCols, kRows;
	int  i,j,m,mm,n,nn,ii,jj;
	float in[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	int rows ,cols ;
	int element_count = 1;
	rows = matrix_size/2;
	cols = rows;

	clear_matrix(in, matrix_size, 1);


	for(i=0;i<rows;++i){
		for(j=0;j<cols;++j){
			in[i][j] = element_count;
			element_count ++;
		}
	}

	kCols = matrix_size;
	kRows = matrix_size;

	kCenterX = kCols /2;
	kCenterY = kRows /2;

	for(i=0; i < rows; ++i)              // rows
	{
	    for(j=0; j < cols; ++j)          // columns
	    {
	        for(m=0; m < kRows; ++m)     // kernel rows
	        {
	            mm = kRows - 1 - m;      // row index of flipped kernel

	            for(n=0; n < kCols; ++n) // kernel columns
	            {
	                nn = kCols - 1 - n;  // column index of flipped kernel

	                // index of input signal, used for checking boundary
	                ii = i + (m - kCenterY);
	                jj = j + (n - kCenterX);

	                // ignore input samples which are out of bound
	                if( ii >= 0 && ii < rows && jj >= 0 && jj < cols )
	                    result_matrix[i][j] += in[ii][jj] * source[mm][nn];
	            }
	        }
	    }
	}
}

void clear_matrix(float source_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size,int value){
	int i, j;

	for(i=0;i<matrix_size;++i)
		for(j=0;j<matrix_size;++j)
			source_matrix=value;
}


void convolution_all_matrix(float result_matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE],float source[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE], int matrix_size){
	int i = 0;
	int j = 0;
	int k = 0;
	int t = 0;

	int half_matrix_size = matrix_size / 2;

	float result1[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float result2[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float result3[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float result4[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];

	float source1[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float source2[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float source3[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
	float source4[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];


	// first quarter copy
	for (i=0; i<half_matrix_size; ++i){
		for (j=0; j<half_matrix_size; ++j){
			source1[i][j] = source[i][j];
		}
	}

	// second quarter copy
	for (i=0; i<half_matrix_size; ++i){
		for (j=half_matrix_size; j<matrix_size; ++j){
			source2[i][j-half_matrix_size] = source[i][j];
		}
	}

	// third quarter copy
	for (i=half_matrix_size; i<matrix_size; ++i){
		for (j=0; j<half_matrix_size; ++j){
			source3[i-half_matrix_size][j] = source[i][j];
		}
	}

	// fourth quarter copy
	for (i=half_matrix_size; i<matrix_size; ++i){
		for (j=half_matrix_size; j<matrix_size; ++j){
			source4[i-half_matrix_size][j-half_matrix_size] = source[i][j];
		}
	}
	// calculate inverse matrix
	convolution_matrix(result1, source1, half_matrix_size);
	convolution_matrix(result2, source1, half_matrix_size);
	convolution_matrix(result3, source1, half_matrix_size);
	convolution_matrix(result4, source1, half_matrix_size);


	//merge all result4
	// first quarter copy
	for (i=0; i<half_matrix_size; ++i){
		for (j=0; j<half_matrix_size; ++j){
			result_matrix[i][j] = result1[i][j];
		}
	}

	// second quarter copy
	for (i=0; i<half_matrix_size; ++i){
		for (j=half_matrix_size; j<matrix_size; ++j){
			result_matrix[i][j] = result2[i][j - half_matrix_size];
		}
	}

	// third quarter copy
	for (i=half_matrix_size; i<matrix_size; ++i){
		for (j=0; j<half_matrix_size; ++j){
			result_matrix[i][j] = result3[i - half_matrix_size][j];
		}
	}

	// fourth quarter copy
	for (i=half_matrix_size; i<matrix_size; ++i){
		for (j=half_matrix_size; j<matrix_size; ++j){
			result_matrix[i][j] = result3[i-half_matrix_size][j- half_matrix_size];
		}
	}
}
