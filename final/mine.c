#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <signal.h>
#include <semaphore.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>


/**
*	MACROS
**/
#define PERMISSION O_CREAT | O_RDWR, S_IRWXU | S_IRWXG
#define PERMISSIONSEM O_CREAT, S_IRUSR | S_IWUSR
#define SHMOBJ_PATH         "/shmMine"
#define SHMARR_PATH         "/shmMineArray"
#define ARRAYSIZE  5000
#define BUFFERSIZE 255
#define TRUE 1
#define FALSE -1
#define MILLION 1000000L
/**
*	For semaphore
**/
sem_t * sem_id;
sem_t * sem_id2;

/**
*	for directory array
**/
struct directory{
	char direcArr[ARRAYSIZE][BUFFERSIZE];
	int size;
};

/**
*	for client connection
**/
struct  shared_data{
	int pid;
	int value;
};


struct  shared_array{
	char  array[BUFFERSIZE];
	int size;
};
struct word{
	char word[BUFFERSIZE];
	int size;
	int point;
};

typedef struct {
	struct word words[ARRAYSIZE];
	int size;


}wordArr;


/**
*	Source
**/
char source[ARRAYSIZE][BUFFERSIZE];
int sourceSize=0;
int sourceUsed=0;

char wordPool[ARRAYSIZE][BUFFERSIZE];
int wordPollSize=0;
/**
*	directory Array
**/
struct directory directories[BUFFERSIZE];

int directorySize=0;

char directoryArray[ARRAYSIZE][BUFFERSIZE];
int dirArrSize=0;


pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

/**
*	gelen clientlerin pidlerini tutuyorum
**/
pid_t clientPids[BUFFERSIZE];
int clientPidsSize=0;

/**
*	olusturulan thread havuzu
**/
pthread_t threads[BUFFERSIZE];
int clientNumber=0;
/**
* Find subdirectory of taken root
* @param root directory
* @return succes
**/

/**
*	logfile
**/
FILE *logFile;
char logFileName[255];
int findSubDirectory(char *directoryName);


/**
*	Fill Directory Array From Argumans
*	@param args from user
*	@param size	size of Array
**/
void fillDirectoryArray(char args[ARRAYSIZE][BUFFERSIZE],int size);
int  isContain(char *word);

void *clientComminication(void *arg);

/**
*	Signal handler for CTRL^C
*	@param int signum signal name
**/
void ctrlC(int signum)
{
	int i=0;
	/**
	*	Time
	**/
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
		
	/**
	*	Unlink fifo
	**/
	unlink("connection");
    
    /**
    *	mutex 
    **/
    pthread_mutex_destroy(&lock);
    
    printf("CTR^C ile sonlandirildi. Tid: %ld\n",pthread_self());
    logFile=fopen(logFileName,"a");
    if(logFile==NULL){
    	fprintf(stderr,"Cannot open File: %s\n",logFileName);
    	exit(1);
    }
    fprintf(logFile,"Ctrl^c ile sonlandirildi.\n");
    fprintf(logFile,"	      %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    fclose(logFile);

    for(i=0;i<clientPidsSize;++i)
    	kill(clientPids[i],SIGINT);
    //for(i=0;i<clientNumber;++i)
    //	pthread_join(threads[i],NULL);
   exit(signum);
}




int main(int argc, char ** argv){


	int value;
	
	int shm_size=(1*sizeof( struct shared_data));
	int shmfd;
	int prevPid=0;
	int client_value=0;
	int i,j,k;


	struct shared_data *shared_msg;
	
	char args[ARRAYSIZE][BUFFERSIZE];

	/**
	*	Time
	**/
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	/**
	

	
	/*
		FOR MAIN FIFO
	*/
	int fd_r;
	char connect[BUFFERSIZE];
	int readValue;

	/***
	*	Signal
	***/
	struct sigaction ctrlc;
	ctrlc.sa_handler = ctrlC;
    ctrlc.sa_flags = 0;
	if((sigemptyset(&ctrlc.sa_mask) || sigaction(SIGINT, &ctrlc, NULL)) == -1){
		perror("Couldn't set signal handler for SIGINT");
		return 1;
	}

	 if(argc ==1){
	 	fprintf(stderr,"Usage error\n");
	 	return 0;
	 }
	 for(i=1;i<argc;++i){
	 	getcwd(args[i-1],255);		//get current working directory
		strcat(args[i-1],"/");
		strcat(args[i-1],argv[i]);
		strcpy(directoryArray[dirArrSize++],args[i-1]);
		
	}
	sprintf(logFileName,"ReportServer%d.log",getpid());
	fillDirectoryArray(args,argc-1);
	 /**
	 * Semaphore open
	 */
	sem_id=sem_open("/mysem", PERMISSIONSEM, 1);
	sem_id2=sem_open("/mysem2", PERMISSIONSEM, 1);
	/**
	*	Channel for first connection with client
	**/
	/*shmfd=shm_open(SHMOBJ_PATH,PERMISSION);
	if(shmfd<0 ){
		fprintf(stderr,"Shared memory cannot create.\n");
		return 0;
	}*/

	if(pthread_mutex_init(&lock,NULL) != 0){
		perror("Mutex init failed");
		return FALSE;

	}

	printf("-----------------Server started-----------------\n");
	printf("________________________________________________\n");
	logFile=fopen(logFileName,"a");
    if(logFile==NULL){
    	fprintf(stderr,"Cannot open File: %s\n",logFileName);
    	exit(1);
    }
	fprintf(logFile,"-----------------Server started-----------------\n");
	fprintf(logFile,"	      %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	fprintf(logFile,"________________________________________________\n");
	fclose(logFile);
	

	//ftruncate(shmfd,shm_size);
	

	//shared_msg = (struct shared_data *)mmap(NULL, shm_size, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);

	//prevPid=shared_msg->pid;
    if (mkfifo("connection",0777) == -1) {

    	perror("failed mkfifo\n"); 
    	unlink("connection");
    	return FALSE;
	}
	shared_msg=(struct shared_data  *)malloc(sizeof(struct shared_data ));

	while(1){

			/**************************************************
			*				CONNECTION NEW CLIENT
			***************************************************/
			if((fd_r = open("connection", O_RDONLY)) == -1){
			fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n","connection");
			return FALSE;
			}
			while((readValue=read(fd_r,connect,BUFFERSIZE))>0);
			//sem_wait(sem_id);
			
			shared_msg->pid = atoi(strtok(connect," "));
			clientPids[clientPidsSize++]=shared_msg->pid;
			shared_msg->value=atoi(strtok(NULL," "));
			client_value=shared_msg->value;
			printf("Client connected pid: %d value : %d\n",shared_msg->pid, shared_msg->value);

			//sem_post(sem_id);

			//sem_wait(sem_id);
			pthread_create(&threads[clientNumber],NULL,clientComminication,(void *)shared_msg);

			//pthread_join(threads[clientNumber],NULL);

			pthread_mutex_destroy(&lock);


			clientNumber++;
			close(fd_r);

		
	}	
	
    /**
     * Semaphore Close: Close a named semaphore
     */
    if ( sem_close(sem_id) < 0 )
    {
    	perror("sem_close");
    }

    /**
     * Semaphore unlink: Remove a named semaphore  from the system.
     */
    if ( sem_unlink("/mysem") < 0 )
    {
    	perror("sem_unlink");
    }	
}


void *clientComminication(void *arg){
	struct shared_data *shm_data;
	struct directory *shm_array;
	struct directory *shm_direcArray;
	int shmfdArray,shmfdDirec;
	char directToServer[255];
	char sharedArrayClient[BUFFERSIZE];
	int i=0;
	char **ptr;
	// time
	long timedif1;
	struct timeval tpend1;
	struct timeval tpstart1;
	/**
	*	Time
	**/
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
		
	/**
	*	For word array shared
	**/
	wordArr *wA;
	int shmWordArray;
	char shm_word_name[255];
	//sem_wait(sem_id2);
	pthread_mutex_lock(&lock);
	shm_data=(struct shared_data *)arg;
	logFile=fopen(logFileName,"a");
	if(logFile==NULL){
		fprintf(stderr,"Cannot open File: %s\n",logFileName);
		exit(1);
	}

	if (gettimeofday(&tpstart1, NULL)) {
		fprintf(stderr, "Failed to get start time\n");
		exit(-1);
	}
	fprintf(logFile,"-----------------Client Connectted-----------------\n");
	fprintf(logFile,"%d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	fprintf(logFile,"________________________________________________\n");
	fprintf(logFile,"Client pid: %d  Request entity : %d\n",shm_data->pid,shm_data->value);
	fclose(logFile);
	sprintf(sharedArrayClient,"/shmMineArray%d",shm_data->pid);
	
	shmfdArray= shm_open(sharedArrayClient,PERMISSION);
	if(shmfdArray<0 ){

		fprintf(stderr,"Shared memory cannot create.\n");
		return 0;
	}
	ftruncate(shmfdArray,sizeof(struct directory)*1);

	shm_array=(struct directory *)mmap(NULL, sizeof(struct directory), PROT_READ | PROT_WRITE, MAP_SHARED, shmfdArray, 0);
	shm_array->size=0;
	/*Data is packing */
	logFile=fopen(logFileName,"a");
	if(logFile==NULL){
		fprintf(stderr,"Cannot open File: %s\n",logFileName);
		exit(1);
	}
	fprintf(logFile,"Entity boyutu %d Kullanılan entity: %d istenilen entity: %d\n",sourceSize,sourceUsed,shm_data->value);
	fclose(logFile);

	for(i=sourceUsed;i<sourceUsed+shm_data->value && i<sourceSize;++i){
		//printf("%s\n",source[i]);
		strcpy(shm_array->direcArr[i-sourceUsed],source[i]);
		//printf("--->>%s\n",shm_array->direcArr[i-sourceUsed]);
		shm_array->size++;
	}


	sourceUsed+=shm_data->value;				
	sleep(1);
	
	/**
	*	Take Directory names
	**/
	
	sprintf(directToServer,"%sA",sharedArrayClient);
	//printf("%s\n",directToServer);
	shmfdDirec= shm_open(directToServer,PERMISSION);
    if(shmfdDirec<0 ){

		fprintf(stderr,"Shared memory cannot create.\n");
		return 0;
	}

	ftruncate(shmfdDirec,sizeof(struct directory));	
	shm_direcArray= (struct directory *)mmap(NULL, sizeof(struct directory), PROT_READ | PROT_WRITE, MAP_SHARED, shmfdDirec, 0);
	sleep(1);
	//sem_wait(sem_id);
	//printf("%d\n",shm_direcArray->size);
	//printf("_____________%d_________________________\n",shm_direcArray->size);
	for(i=0;i<shm_direcArray->size;++i){
		strcpy(directoryArray[dirArrSize++],shm_direcArray->direcArr[i]);
		//printf("--------->>direc: %s\n",shm_direcArray->direcArr[i]);
	}
	fillDirectoryArray(shm_direcArray->direcArr,shm_direcArray->size);
	

	sprintf(shm_word_name,"/shm_w_%d",shm_data->pid);
	shmWordArray= shm_open(shm_word_name,PERMISSION);

    if(shmWordArray<0 ){

		fprintf(stderr,"Shared memory cannot create.\n");
		return 0;
	}
	//printf("%s\n",shm_word_name);
	
	ftruncate(shmWordArray,sizeof(wordArr));


    wA= (wordArr*)mmap(NULL, sizeof(wordArr), PROT_READ | PROT_WRITE, MAP_SHARED, shmWordArray, 0);
    sleep(1);
    printf("Wa size: %d\n",wA->size);
    for(i=0;i<wA->size;++i){
    	if(isContain(wA->words[i].word)!=-1){
    		wA->words[i].point=wA->words[i].size*1;

    	}else{

    		strcpy(wordPool[wordPollSize++],wA->words[i].word);
    		wA->words[i].point=10*wA->words[i].size;
    	}
    	
    }
	if (gettimeofday(&tpend1, NULL)) {
		fprintf(stderr, "Failed to get end time\n");
		exit(-1);
	}

	timedif1 = MILLION*(tpend1.tv_sec - tpstart1.tv_sec) +tpend1.tv_usec - tpstart1.tv_usec;
	logFile=fopen(logFileName,"a");
	if(logFile==NULL){
		 	fprintf(stderr,"Cannot open File: %s\n",logFileName);
	    	exit(1);
	 }
	fprintf(logFile,"Tid: %ld  Gecen Sure: %ld \n",pthread_self(),timedif1);	
	fclose(logFile);
	//sem_post(sem_id);

	pthread_mutex_unlock(&lock);

}
/*****************************************************************************
*				________ 				_______    ________	   _______
*	|	   |   |	       |			|	   |  | 		  |		  |
*	|	   |   |		   |			|	   |  |			  | 	  |
*	|______|   |------	   |			|______|  |------	  |	______|
*	|	   |   |		   |			|		  |			  |	- _
*	|	   |   |_______    |_______ 	|		  |________   |		- _
*
*
*
*********************************************************************************/
int findSubDirectory(char *directoryName){

	struct dirent *direntp;
	DIR    *dirp;
	char   *strNewPath;
	char    *strFilePath;
	int 	iDirSize;
	char *strRoot;

	/*gerekli yerleri kopyaliyorum*/
	strNewPath=(char *)malloc(sizeof(char)*255);
	strFilePath=(char *)malloc(sizeof(char)*255);
	strRoot=(char *)malloc(sizeof(char)*255);


	strcpy(strNewPath,directoryName);
	/*directoryi aciyorum acilamazsa hata veriyorum*/
	if ((dirp = opendir(strNewPath)) == NULL) {
			fprintf (stderr, "Failed to open directory \"%s\" : %s\n",
			         strNewPath, strerror(errno));

			exit(0);
	}
	while((direntp=readdir(dirp))!=NULL){//directoryde eleman kalmayana kadar okuma yapıyorum

	//	if(direntp->d_type & DT_DIR)//klosorse
	//	{
			
				iDirSize=strlen(direntp->d_name);/*dosya ad uzunlugunu alip .. veya . olmamasına dikkat ediyorum yoksa beni sürekli geri atar*/
				
				
				if(iDirSize==2 && direntp->d_name[0]=='.' && direntp->d_name[1]=='.'){
						
				}else if(iDirSize==1 && direntp->d_name[0]=='.'  || direntp->d_name[iDirSize-1] == '~' ){
					
				}else
				{	
					
					strcpy(strRoot,strNewPath);/*yeni klasorle beraber yeni pathi beirliyorum ve recursive bi sekilde bitene kadar yapiyorum*/
					strcat(strRoot,"/");
					strcat(strRoot,direntp->d_name);
					if(strlen(strRoot)<=255){/*Unix path uzunlugunu 255 den fazla izin vermedigi icin kontrol ediyorum*/
						strcpy(directories[directorySize].direcArr[directories[directorySize].size],strRoot);
						//printf("%s\n",directories[directorySize].direcArr[directories[directorySize].size]);
						strcpy(source[sourceSize++],directories[directorySize].direcArr[directories[directorySize].size]);
						directories[directorySize].size++;
						
					}
					else{
						printf("Dosya isim boyutu asti\nProgram kapatiliyor.\n");
						break;
					}
			
				}

	}
	free(strNewPath);//mallocla aldıgım yerleri geri veriyorum
	free(strFilePath);
	free(strRoot);
	if (closedir (dirp)) {//dosyayı kapatiyorum
		fprintf (stderr, "'%s:	dosyasi kapatilamadi %s\n",directoryName, strerror (errno));
   		 exit (EXIT_FAILURE);		
		}


}

void fillDirectoryArray(char args[ARRAYSIZE][BUFFERSIZE],int size){

	int i,j,k;
	char direc[BUFFERSIZE];

	//directorySize=0;

	for(k=0;k<size;++k){

		//printf("__________________________________________\n");
		findSubDirectory(args[k]);
			
		
	}

}
int  isContain(char *word){

	int i=0;
	for(i=0;i<wordPollSize;++i)
	{
		if(strcmp(wordPool[i],word)==0)
			return i;
	}	
	return FALSE;
}