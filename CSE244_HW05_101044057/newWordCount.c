/**
*	@author Hakan DEGIRMEN Num:101044057
*	@version v.1.0
*	@since 07.05.2015
*/

/**
*	         COMPILER & RUN
*		_________________________
*	
*	*	gcc -c newWordCount.c
*  	*	gcc wordCount.o -o newWordCount -lpthread
*	*	./newWordCount <directoryName>
*
***/


/****************************************************
*					KUTUPHANELER
*****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#define MILLION 1000000L

/**
*	typedef struct newWord bu structure kelime ve kac tane oldugunu tutar
*	@param word kelime
*	@param number o kelimeden kac tane oldugu
**/
typedef struct newWord{
	char word[255];
	int number;

}newWord;

/**
*	Bu fonksiyon verilen pathdeki
*	tüm klasor ve dosyaları bulma
*	ya yarar.
*
	*@param string Path
**/
void dizinDolas(char *strDirName);

/**
*		Verilen pathin klasor olup olmadigini dönderir
*		@param string Path
*		@return int isDirectory path klasorse modunu degilse 0 donderir.
**/
int isdirectory(char *path);

/**
*	Verile file adında kelime sayacak olan fonksiyondur.
*	@param fileName kelime sayilacak dosyanin adi
*	
**/
void search(char *strDirName);

/**
*	Verilen bir satirda kac tane gecerli kelime var onu sayacal
*	@param string line  source line
*	@return int count of valid word
**/
int searchLine(char chSatir[]);


/**
*	Verilen kelimenin daha onceden var olup olmadigina bakar
*	Var ise listedeki indexi donderir yoksa -1 donderir
*	@param key string 
*	@return verilen stringin hangi indexde oldugunu gosterir yoksa -1 donderir
**/

int isContain(char key[]);
/**
*	Filede buldugu gecerli kelimeleri fifoya yazacak
*	@param fileName source file
*
**/
void * traceFile(void * fName); 


/**
*	Parentin okudugu kelimeleri bir newWord
*	structi halinde arraya doldurur 
**/
void fillArr();






/**
*			GLOBAL VARIABLES
**/
int wordSize=0;
int wordArraySize=0;
newWord wordArr[150005];
char fileNames[255][255];
int fileNumber=0;
int totalDirectory=0;



/**
*	Thread mutex
**/

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

/**
*	Main Function Driver function
*	
*	@param int argc number of arguments   
*	@param char** argv  arguments
*	@return success
**/

int main(int argc, char ** argv) {


	char strDirName[255];
	int i;
	char strDir[255];
	FILE *reportFile;
	pthread_t threads[180];
	// time
	long timedif;
	struct timeval tpend;
	struct timeval tpstart;

	/*############################*
	#			USAGE
	##############################*/
	
	if(argc != 2){
		printf("Usage:./newWordCount <directoryName>\nProgram is closing...");
		return 0;
	}else{
		strcpy(strDirName,argv[1]);/*Argumanlari her ihtimale karsi kopyaliyorum*/	
		getcwd(strDir,255);		//get current working directory
		strcat(strDir,"/");
		strcat(strDir,argv[1]);//yeni directoryi ayarliyorum
		if(!isdirectory(strDir)){//directory yoksa kapatiliyor
			printf("Boyle bir directory yok program kapatiliyor.\n");
			return 0;
		}
		if (gettimeofday(&tpstart, NULL)) {
			fprintf(stderr, "Failed to get start time\n");
			exit(-1);
		}
		dizinDolas(strDir);//duzgun ise recursive bi sekilde icindekileri listeyeyecek
		if (gettimeofday(&tpend, NULL)) {
			fprintf(stderr, "Failed to get end time\n");
			exit(-1);
		}
		
		timedif = MILLION*(tpend.tv_sec - tpstart.tv_sec) +tpend.tv_usec - tpstart.tv_usec;
		printf("Klasor adi : %s Gecen Sure: %u \n",strDir,timedif);
		for(i=0;i<fileNumber;++i){
			
			pthread_create(&threads[i],NULL,traceFile,(void *)fileNames[i]);	
		}
		for(i=0;i<fileNumber;++i){
			pthread_join(threads[i],NULL);	
		}
		pthread_mutex_destroy(&lock);
		fillArr();
	
		/*Bilgileri report.log dosyasina kaydediyorum*/
		if((reportFile=fopen("Report.log","w"))==NULL){
			fprintf(stderr,"Cannot open Report.log file\nProgram is closing...\n");
			return 0;

		}
		for(i=0;i<wordArraySize;++i)
			fprintf(reportFile,"\t%s   ---------->%d\n",wordArr[i].word,wordArr[i].number);	
		
		printf("Total Directory: %d\nTotal File: %d\nTotal Different Word: %d\n",totalDirectory,fileNumber,wordArraySize);
		fprintf(reportFile,"____________________________________________________________\n");
		fprintf(reportFile,"Total File:%d   Total Different Word:%d\n",fileNumber,wordArraySize);

		fclose(reportFile);
		unlink("HW05Result.txt");
		return 0;
		
	}
	
	
}/*main End*/



/**
*	Bu fonksiyon verilen pathdeki
*	tüm klasor ve dosyaları bulma
*	ya yarar.
*
*	@param string Path
**/

void dizinDolas(char *strDirName){

	
		
	// time
	long timedif;
	struct timeval tpend;
	struct timeval tpstart;

	struct dirent *direntp;
	DIR    *dirp;
	char   *strNewPath;
	char    *strFilePath;
	int 	iDirSize;
	char *strRoot;
	/*gerekli yerleri kopyaliyorum*/
	strNewPath=(char *)malloc(sizeof(char)*255);
	strFilePath=(char *)malloc(sizeof(char)*255);
	strRoot=(char *)malloc(sizeof(char)*255);

	strcat(strNewPath,strDirName);
	/*directoryi aciyorum acilamazsa hata veriyorum*/
	
	if ((dirp = opendir(strDirName)) == NULL) {
			fprintf (stderr, "Failed to open directory \"%s\" : %s\n",
			         direntp->d_name, strerror(errno));
			exit(0);
	}
	while((direntp=readdir(dirp))!=NULL){//directoryde eleman kalmayana kadar okuma yapıyorum

		if(direntp->d_type & DT_DIR)//klosorse
		{
			
				iDirSize=strlen(direntp->d_name);/*dosya ad uzunlugunu alip .. veya . olmamasına dikkat ediyorum yoksa beni sürekli geri atar*/
				
				
				if(iDirSize==2 && direntp->d_name[0]=='.' && direntp->d_name[1]=='.'){
						
				}else if(iDirSize==1 && direntp->d_name[0]=='.' ){
					
				}else
				{	
					totalDirectory++;
					strcpy(strRoot,strNewPath);/*yeni klasorle beraber yeni pathi beirliyorum ve recursive bi sekilde bitene kadar yapiyorum*/
					strcat(strRoot,"/");
					strcat(strRoot,direntp->d_name);
					
					if(strlen(strRoot)<=255){/*Unix path uzunlugunu 255 den fazla izin vermedigi icin kontrol ediyorum*/
						if (gettimeofday(&tpstart, NULL)) {
								fprintf(stderr, "Failed to get start time\n");
								exit(-1);
							}
							dizinDolas(strRoot);//duzgun ise recursive bi sekilde icindekileri listeyeyecek
							if (gettimeofday(&tpend, NULL)) {
								fprintf(stderr, "Failed to get end time\n");
								exit(-1);
							}

							timedif = MILLION*(tpend.tv_sec - tpstart.tv_sec) +tpend.tv_usec - tpstart.tv_usec;
							printf("Klasor adi : %s Gecen Sure: %u \n",strRoot,timedif);	
					}
					else{
						printf("Dosya isim boyutu asti\nProgram kapatiliyor.\n");
						break;
					}
			
				}
					
				

				
		}
		else{//dosya ise
			
			iDirSize=strlen(direntp->d_name);

		
			if(direntp->d_name[iDirSize-1] != '~'){
				strcpy(strFilePath,strDirName);/*Yeni dosya ile beraber yeni path buluyorum*/
				strcat(strFilePath,"/");
				strcat(strFilePath,direntp->d_name);
				if(strlen(strFilePath)<=255){//arama fonksiyonunu gonderiyorum
					
					strcpy(fileNames[fileNumber++],strFilePath);
					
				}
				else{
					printf("Dosya isim boyutu asti\nProgram kapatiliyor.\n");
					break;
				}
				
			}
			

		}
		
		
	}
	free(strNewPath);//mallocla aldıgım yerleri geri veriyorum
	free(strFilePath);
	free(strRoot);
	if (closedir (dirp)) {//dosyayı kapatiyorum
		fprintf (stderr, "'%s:	dosyasi kapatilamadi %s\n",strDirName, strerror (errno));
   		 exit (EXIT_FAILURE);		
		}
	

}/*DizinDolas end*/




void * traceFile(void * fName) {

	FILE *file;
	char word[255];
	char *line;
	int offset;
	int totalOffset=0;
	int i=0;
	int status;
	FILE *resFile;
	char fileName[255];
	// time
	long timedif;
	struct timeval tpend;
	struct timeval tpstart;
	//mutexlock
	pthread_mutex_lock(&lock);
	if (gettimeofday(&tpstart, NULL)) {
		fprintf(stderr, "Failed to get start time\n");
		exit(-1);
	}
	strcpy(fileName,(char *)fName);
	resFile=fopen("HW05Result.txt","a");
	/*Verilen dosyayi acip kelimeleri ayirip gecerli olanlari verilen fifoya yazar*/
	if((file=fopen(fileName,"r"))==NULL){
		fprintf(stderr,"Dosya acilamadi.%s\n",fileName);
		exit (EXIT_FAILURE);

	}
	
	line =(char *)malloc(sizeof(char)*255);
	while(!feof(file) && fgets(line,255,file) != NULL){
		while(sscanf(line,"%s%n",word,&offset)==1){
			
			for(i=0;i<strlen(word);++i){
				if(word[i]>=65 && word[i]<=122){
					if(word[i]>=91 && word[i]<=96)
						status=-1;
				}else{
					status=-1;
				}

			}
			if(status!=-1){
				
				fprintf(resFile,"%s\n",word);
				wordSize++;
			}
			status=0;

			line+=offset;
			//totalOffset+=offset;
			
		}
	}
	
	fclose(file);
	fclose(resFile);

	//mutex unlock
	if (gettimeofday(&tpend, NULL)) {
		fprintf(stderr, "Failed to get end time\n");
		exit(-1);
	}

	timedif = MILLION*(tpend.tv_sec - tpstart.tv_sec) +tpend.tv_usec - tpstart.tv_usec;
	printf("Tid: %u Dosya adi : %s Gecen Sure: %u \n",pthread_self(),fileName,timedif);	
	pthread_mutex_unlock(&lock);
	return (void*)1;
	
}

void fillArr(){

	FILE *reportFile;
	char *line;
	int index;
	reportFile=fopen("HW05Result.txt","r");
	
	line=(char *)malloc(sizeof(char)*255);
	
	while(!feof(reportFile) && fgets(line,255,reportFile) !=NULL){
		line[strlen(line)-1]='\0';
		index=isContain(line);
		if(index != -1){

			wordArr[index].number++;
		}
		else{

			strcpy(wordArr[wordArraySize].word,line);
			wordArr[wordArraySize++].number=1;

		}
			
	}

	fclose(reportFile);
	free(line);
	

}


int isContain(char key[]){
	int i=0;
	for(;i<wordArraySize;++i){
		if(strcmp(wordArr[i].word,key)==0){
			return i;
		}

	}
	return -1;


}
int isdirectory(char *path) {
   struct stat statbuf;

   if (stat(path, &statbuf) == -1)
      return 0;
   else
      return S_ISDIR(statbuf.st_mode);
}
