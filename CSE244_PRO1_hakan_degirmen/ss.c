/**
*	@author Hakan DEGIRMEN Num:101044057
*	@version v.1.0
*	@since 15.04.2015
*/

/**
*	         COMPILER & RUN
*		_________________________
*	
*	*	gcc -c ss.c
*  	*	gcc ss.o -o ss
*	*	./ss <mainFifoName>
*
***/


/****************************************************
*					KUTUPHANELER
*****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>
#include <math.h>


/****************************************************
*					GLOBAL VARIABLES
*****************************************************/
#define BUFFERSIZE 	255
#define PROCESSSIZE   15000
#define PERMISSION 	0777
#define TRUE 		1
#define FALSE 		0
#define SUCCESS 	1
#define UNSUCCESS 	-1
#define EXCEPTIONNUMBER 5

/*************************
*		FUNCTIONS
**************************/

/**
*	Kullaniciya programi nasil calistirmasi gerektigini soyler
**/
void usage();
/**
*	Exception mesajlarini ayarliyorum
***/
void setting();
/**
*	Pipedan okuma fonksiyonu
*	@param file nerden okuyacagi
*	@param buffer nereye okuyacagi
**/
void read_from_pipe (int file,char buffer[BUFFERSIZE]);
/**
*	Pipe a yazma fonksiyonu
*	@param file nereye yazacagi
*	@param buffer neyi yazacagi
**/
void write_to_pipe (int file,char message[BUFFERSIZE]);

/**
*	(a^2+b^2)/|c| islemini hesaplayan fonksyiyon c == 0 icin -1 donderir islem yapmaz
*	@param a 1.deger
*	@param b 2.deger
*	@param c 3.deger
*	@return result fonksiyonun sonucunu hesaplar c == 0 ise -1 donderir division by zero onlenir
**/
double operation1(double a,double b,double c);

/**
*	(a+b)^(1/2) islemini hesaplayan fonksiyon  a+b<0 icin -1 donderir
*	@param a 1.deger
*	@param b 2.deger
*	@return result fonksiyonun sonucunu hesaplar a+b<0 ise -1 donderir division by zero onlenir
**/
double operation2(double a,double b);

/**
*	Serverin calismaya basladigi fonksiyon
*	@param haberlesilecek fifo adi
*	@return success
**/
int startSServer(char fifoName[BUFFERSIZE]);

/*************************
*		Typedefs
**************************/
typedef char * chPtr;
typedef int	 * intPtr;








/**
*	Exception mesajlarini sakladigim array
**/
char exceptions[EXCEPTIONNUMBER][BUFFERSIZE];

/**
*	CTRL^C islemi icin gerekli yedekleme icin tutuyorum 
**/
pid_t childProcess;
int childProcesses[PROCESSSIZE];
int clientPid[PROCESSSIZE];
char operations[PROCESSSIZE][BUFFERSIZE];

/*For MathServerFifo*/
char mathServerFifoName[BUFFERSIZE];
char mathServerBuffer[BUFFERSIZE];

/*Other*/
int clientNumber=0;

int mathServerTimeFlag=0;

char ffName[BUFFERSIZE];

char reportFileName[BUFFERSIZE];

/*For FILES*/
FILE *reportFile;

/*Math Server 30 sn kontrolu*/
void  ALARMhandler(int sig)
{
  signal(SIGALRM, SIG_IGN);          /* ignore this signal       */
  printf("TimeOut %d %d\n",getpid(),clientPid[clientNumber-1]);
  /* kill(childProcesses[clientNumber-1],SIGKILL);
  kill(clientPid[clientNumber-1],SIGKILL);*/

  signal(SIGALRM, ALARMhandler);     /* reinstall the handler    */

  mathServerTimeFlag=1;

}
	
/*CTRL^C perent prosese mi mathservera mi geldigini belirlemek icin*/
int flag=0;

static void ctrlC(int signo) {
    char ff[BUFFERSIZE];
	int i=0;
	fprintf(stderr,"CTR^C is detected. Pid: %d\n",getpid());
    if(flag==0){//parent
    	for(i=0;i<clientNumber;++i){
    		kill(clientPid[i],SIGINT);

    	}
    	reportFile=fopen(reportFileName,"a");
    	fprintf(reportFile,"CTR^C is detected.\n");
    	fclose(reportFile);
    	unlink(ffName);
   		exit(0);
   	}else{//child
   		sprintf(ff,"fifo_%d",getpid());
   		unlink(ff);
   		fclose(reportFile);
   		exit(1);

   	}

   	//}
	exit (-1);
} 
int main(int argc,char** argv){
	

	if(argc != 2){
		usage();
		return UNSUCCESS;
	}else{
		setting();
		strcpy(ffName,argv[1]);
		startSServer(argv[1]);
		
	
	}


}

void usage(){
	fprintf(stderr,"Usage:\n\t./server <mainFifoName>\n");

}

 void read_from_pipe (int file,char buffer[BUFFERSIZE])
 {
   FILE *stream;
   int c,i=0;
   stream = fdopen (file, "r");
   while ((c = fgetc (stream)) != EOF){
     buffer[i]=c;
     i++;
   }
   buffer[i]='\0';
   fclose (stream);
 }
void write_to_pipe (int file,char message[BUFFERSIZE])
{
  FILE *stream;
  
  stream = fdopen (file, "w");
  fprintf(stream,"%s",message);
  fclose (stream);
}

void setting(){
	strcpy(exceptions[0],"Division by Zero");
	strcpy(exceptions[1],"Sum of parameters is negative");
	strcpy(exceptions[2],"Delta is negative");
	strcpy(exceptions[3],"Inverse of the function is undefined");
	strcpy(exceptions[4],"A is negative");

}

int startSServer(char fifoName[BUFFERSIZE]){

	int  fd,fd_r,fd_w,i;
	int	 readValue=0;
	char buffer[BUFFERSIZE];

	
	int ms_fd_r;
	int ms_fd_w;

	/*For pipe*/
	int fdPipe[PROCESSSIZE][2];
	char pipeBuffer[BUFFERSIZE];
	int nbytes=0;


	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	/*For parameters*/
	int waitTime;
	int paramNumber;
	double paramValues[12];
	char *token;
	int k=0;
	double result;
	double root1,root2;
	double inv;
	
	struct sigaction ctrlc;
	ctrlc.sa_handler = ctrlC;
    ctrlc.sa_flags = 0;
	if((sigemptyset(&ctrlc.sa_mask) || sigaction(SIGINT, &ctrlc, NULL)) == -1){
		perror("Couldn't set signal handler for SIGINT");
		return 1;
	}

	
	
	sprintf(reportFileName,"ReportFile_%s.log",fifoName);
	

	signal(SIGALRM, ALARMhandler);
	
	if (mkfifo(fifoName,PERMISSION) == -1) {

        	perror("failed mkfifo\n"); 
        	unlink(fifoName);
        	return UNSUCCESS;
	}
	printf("		SERVER STARTED		\n");
	printf("	      %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	printf("__________________________________________________\n");

	while(1){

		
		if((fd_r = open(fifoName, O_RDONLY)) == -1){
			fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",fifoName);
			return UNSUCCESS;
		}
		while((readValue=read(fd_r,buffer,BUFFERSIZE))>0);
		printf("%d.Client connected.\n",clientNumber+1);
		close(fd_r);
		clientPid[clientNumber]=atoi(strtok(buffer," "));
		strcpy(operations[clientNumber],strtok(NULL,"\0"));
		t=time(NULL);
		tm = *localtime(&t);
		
		clientNumber++;
		strcpy(mathServerFifoName,"\0");
		 /* Create the pipe. */
    	if (pipe (fdPipe[clientNumber-1]))
	    {
		    fprintf (stderr, "Pipe failed.\n");
		    return EXIT_FAILURE;
		 }
		childProcess = fork();

		if(childProcess==-1){

			fprintf ( stderr ,"Cannot create new process.\nProgram is closing.\n");
			return UNSUCCESS;

		}else if(childProcess==0){

			reportFile=fopen(reportFileName,"a");
			//set Alarm
			flag=1;
			alarm(30);
			
			/*Generate mathServer name for each client with process id*/
			childProcesses[clientNumber-1]=getpid();
			sprintf(mathServerFifoName,"fifo_%d",childProcesses[clientNumber-1]);
			
			/*Pipe cominication*/
			close(fdPipe[clientNumber-1][0]);
			write_to_pipe (fdPipe[clientNumber-1][1],mathServerFifoName);
			//write(fdPipe[1],mathServerFifoName,BUFFERSIZE);
			if (mkfifo(mathServerFifoName,PERMISSION) == -1) {
	        	perror("failed mkfifo\n"); 
	        	unlink(mathServerFifoName);
	        	return UNSUCCESS;
			}

			if((ms_fd_r = open(mathServerFifoName, O_RDONLY)) == -1){
				fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",mathServerFifoName);
				return UNSUCCESS;
			}
			
			while((readValue=read(ms_fd_r,mathServerBuffer,BUFFERSIZE))>0);

			close(ms_fd_r);

			paramNumber=0;
			token=strtok(mathServerBuffer," ");
			
			waitTime=atoi(token);
			token=strtok(NULL," ");
			strcpy(operations[clientNumber-1],token);
			

			while(token != NULL){
				token=strtok(NULL," ");
				if(token==NULL)
					break;
				paramValues[paramNumber]=atof(token);					
				paramNumber++;

			}
			tm = *localtime(&t);
			fprintf(reportFile,"__________________________________________________\n");
			fprintf(reportFile,"%d. Client connected\n",clientNumber);
			fprintf(reportFile,"Pid:%d\t\tOperation:%s\t\t%d-%d-%d %d:%d:%d\n",clientPid[clientNumber-1],operations[clientNumber-1],
												 tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

			
			sleep(waitTime);
			
			k=0;
			while(k<paramNumber){
					
					if(strcmp(operations[clientNumber-1],"Operation1")==0){

						if((paramNumber-k) % 3 != 0){
							 sprintf(buffer,"Pid:%d Operation: Operation1 less element",getpid());
							 k=paramNumber;
						}
						else{	
							result = operation1(paramValues[k],paramValues[k+1],paramValues[k+2]);
							
							if(result == UNSUCCESS){
								sprintf(buffer,"Pid:%d Operation: Operation1 A:%.3f B:%.3f C:%.3f %s",getpid(),paramValues[k],paramValues[k+1]
								,paramValues[k+2],exceptions[0]);
							}else{
								sprintf(buffer,"Pid:%d Operation: Operation1 A:%.3f B:%.3f C:%.3f Result:%.3f",getpid(),paramValues[k],paramValues[k+1]
									,paramValues[k+2],result);
							}
							k+=3;
						}
						if((ms_fd_w = open(mathServerFifoName, O_WRONLY)) == -1){
							fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",mathServerFifoName);
							return UNSUCCESS;
						}							
						write(ms_fd_w,buffer,BUFFERSIZE);
						close(ms_fd_w);
						usleep(200);

				}else if(strcmp(operations[clientNumber-1],"Operation2")==0){

						if((paramNumber-k) % 2 != 0){
							 sprintf(buffer,"Pid:%d Operation: Operation2 less element",getpid());
							 k=paramNumber;
						}
						else{	
							result = operation2(paramValues[k],paramValues[k+1]);
							
							if(result == UNSUCCESS){
								sprintf(buffer,"Pid:%d Operation: Operation2 A:%.3f B:%.3f %s",getpid(),paramValues[k],paramValues[k+1]
								,exceptions[1]);
							}else{
								sprintf(buffer,"Pid:%d Operation: Operation2 A:%.3f B:%.3f Result:%.3f",getpid(),paramValues[k],paramValues[k+1]
									,result);
							}
							k+=2;
						}
						if((ms_fd_w = open(mathServerFifoName, O_WRONLY)) == -1){
							fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",mathServerFifoName);
							return UNSUCCESS;
						}							
						write(ms_fd_w,buffer,BUFFERSIZE);
						close(ms_fd_w);
						usleep(200);

				}else if(strcmp(operations[clientNumber-1],"Operation3")==0){

						if((paramNumber-k) % 3 != 0){
							 sprintf(buffer,"Pid:%d Operation: Operation3 less element",getpid());
							 k=paramNumber;
						}
						else{	
							if(paramValues[k]==0){
								sprintf(buffer,"Pid:%d Operation: Operation3 A:%.3f B:%.3f C:%.3f %s",getpid(),paramValues[k],paramValues[k+1],paramValues[k+2]
									,exceptions[4]);
								k+=3;
							}else{
								result=(pow(paramValues[k+1],2)-4*paramValues[k]*paramValues[k+2]);
							
								
								if(result < 0){
									sprintf(buffer,"Pid:%d Operation: Operation3 A:%.3f B:%.3f C:%.3f %s",getpid(),paramValues[k],paramValues[k+1],paramValues[k+2]
									,exceptions[2]);
								}else{
									root1 = (-1*paramValues[k+1]+sqrt(result))/(2*(paramValues[k]));
									root2 = (-1*paramValues[k+1]-sqrt(result))/(2*(paramValues[k]));
									sprintf(buffer,"Pid:%d Operation: Operation3 A:%.3f B:%.3f C:%.3f  Root1:%.3f Root2:%.3f",getpid(),paramValues[k],paramValues[k+1],paramValues[k+2]
										,root1,root2);
								}
								k+=3;
							}
						}
						if((ms_fd_w = open(mathServerFifoName, O_WRONLY)) == -1){
							fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",mathServerFifoName);
							return UNSUCCESS;
						}							
						write(ms_fd_w,buffer,BUFFERSIZE);
						close(ms_fd_w);
						usleep(200);

				}
				else if(strcmp(operations[clientNumber-1],"Operation4")==0){

						if((paramNumber-k) % 4 != 0){
							 sprintf(buffer,"Pid:%d Operation: Operation4 less element",getpid());
							 k=paramNumber;
						}
						else{	
							if(paramValues[k] != paramValues[k+2]){//x=1 alıcam
								inv = (-1*paramValues[k+3]+paramValues[k+1])/(paramValues[k+2]-paramValues[k]);
								result=(paramValues[k]*inv+paramValues[k+1])/(paramValues[k+2]*inv+paramValues[k+3]);

							}else{//x=2 alıcam
								inv = (-1*paramValues[k+3]*2+paramValues[k+1])/(paramValues[k+2]*2-paramValues[k]);
								result=(paramValues[k]*inv+paramValues[k+1])/(paramValues[k+2]*inv+paramValues[k+3]);
							}


							if(result == SUCCESS){

								sprintf(buffer,"Pid:%d Operation4 A:%.3f B:%.3f C:%.3fD: %.3f Inverse : -%.2fX+%.2f/%.2fX-%.2f",getpid(),paramValues[k],
									paramValues[k+1],paramValues[k+2],paramValues[k+3],paramValues[k+3],paramValues[k+1],paramValues[k+2],paramValues[k]);
							}else{
								
								sprintf(buffer,"Pid:%d Operation: Operation4 A:%.3f B:%.3f C:%.3f D:%.3f Inverse: %s",getpid(),paramValues[k],paramValues[k+1]
									,paramValues[k+2],paramValues[k+3],exceptions[3]);
							}
							k+=4;
						}
						if((ms_fd_w = open(mathServerFifoName, O_WRONLY)) == -1){
							fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",mathServerFifoName);
							return UNSUCCESS;
						}							
						write(ms_fd_w,buffer,BUFFERSIZE);
						close(ms_fd_w);

						usleep(200);

				}else{
					fprintf(stderr,"Undefined Operation %s",operations[clientNumber-1]);
					exit(1);
				}

			}
			fprintf(reportFile,"Pid: %d Operation: %s is done before CTRL^C\n",getpid(),operations[clientNumber-1]);
			fclose(reportFile);
	        exit(0);


		}else{

			flag=0;
			
			close(fdPipe[clientNumber-1][1]);

			read_from_pipe (fdPipe[clientNumber-1][0],pipeBuffer);

			

			/*pipe ile alinan fifo adi  mainFifo ile  cliente verildi*/
			if((fd_w = open(fifoName,O_WRONLY))==-1){
				fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",fifoName);
			
				return UNSUCCESS;
			}
			strcpy(buffer,"\0");
			/*send fifoname*/
			sprintf(buffer,"%d %s",clientPid[clientNumber-1],pipeBuffer);
			write(fd_w,buffer,BUFFERSIZE);
			close(fd_w);
			
			
			
			
		}

	}

}

double operation1(double a,double b,double c){	
	if(c==0)
		return UNSUCCESS;
	else{
		return (sqrt((pow(a,2)+pow(b,2)))/abs(c));
	}
}

double operation2(double a,double b){

		if((a+b)<0)
			return UNSUCCESS;
		else
			return sqrt(a+b);
}


