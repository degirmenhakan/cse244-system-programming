/**
*	@author Hakan DEGIRMEN Num:101044057
*	@version v.1.0
*	@since 15.04.2015
*/

/**
*	         COMPILER & RUN
*		_________________________
*	
*	*	gcc -c client.c
*  	*	gcc client.o -o client
*	*	./client <mainFifoName> <waitTime> <operationName> <param1> <param2> .... <paramk>
*
***/


/****************************************************
*					KUTUPHANELER
*****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>
#include <errno.h>



#define BUFFERSIZE 255
void used();

FILE *reportFile;
char fifoName[BUFFERSIZE];
char mathServerFifoName[BUFFERSIZE];
int serverPid;

static void ctrlC(int signo) {
    
	int fd_w;
	char buffer[BUFFERSIZE];
    fprintf(stderr,"\nCtrlC is detected \n");
	fprintf(reportFile,"\nCtrlC is detected \n");
	printf("ServerPid: %d\n",serverPid);
	kill(serverPid,SIGINT);
	exit (-1);
}

int main(int argc,char **argv){

	int fd,fd_r,fd_w;
	char buffer[BUFFERSIZE];
	char operation[BUFFERSIZE];
	
	int pid;
	int ms_fd_r,ms_fd_w;
	char message[BUFFERSIZE];
	int k=0;
	int i,paramNo;
	
	char reportFileName[BUFFERSIZE];
	time_t t = time(NULL);
	struct sigaction ctrlc;


	if(argc>16 || argc<6)
	{
		used();
		return 0;
	}else{

    	ctrlc.sa_handler = ctrlC;
	    ctrlc.sa_flags = 0;
	    if((sigemptyset(&ctrlc.sa_mask) || sigaction(SIGINT, &ctrlc, NULL)) == -1){
			perror("Couldn't set signal handler for SIGINT");
			return -1;
		}

		sprintf(reportFileName,"Report_%d",getpid());
		reportFile=fopen(reportFileName,"w");
			
		strcpy(fifoName,argv[1]);
		strcpy(operation,argv[3]);
		if((fd_w = open(fifoName,O_WRONLY))== -1){
			fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",fifoName);
			return -1;
		}
		
		sprintf(buffer,"%d %s",getpid(),operation);
		write(fd_w,buffer,BUFFERSIZE);
		close(fd_w);
		/***********************************
		*	S. Servera baglandı	Logladim 
		************************************/
		fprintf(reportFile,"Connect to %s\n",fifoName);
		struct tm tm = *localtime(&t);
		fprintf(reportFile,"	      %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
		fprintf(reportFile,"____________________________________________________________\n");
		sprintf(buffer,"%d %s\n",getpid(),operation);

		if((fd_r = open(fifoName,O_RDONLY))==-1){
			fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",fifoName);
			return -1;
		}
		while(read(fd_r,buffer,255)>0);
		printf("FifoName: %s\n",buffer);
		close(fd_r);

		pid=atoi(strtok(buffer," "));
		strcpy(mathServerFifoName,strtok(NULL,"\0"));

		strcpy(message,mathServerFifoName);
		strtok(message,"_");
		serverPid = atoi(strtok(NULL,"_"));
		//printf("server Pid: %d Buffer:%s  | math:%s\n",serverPid,buffer,mathServerFifoName);

		fprintf(reportFile,"mathServer's fifo name is :%s\n",mathServerFifoName);
		if((ms_fd_w = open(mathServerFifoName,O_WRONLY))==-1){
				fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",mathServerFifoName);
				return -1;
		}
		strcpy(buffer,"\0");
		

		for(i = 2;i<argc;++i){
			strcat(buffer,argv[i]);
			strcat(buffer," ");
		}
		
		write(ms_fd_w,buffer,255);
		close(ms_fd_w);

	
		k=0;
		while(1){
			if(strcmp(operation,"Operation1")==0)
				paramNo=3;
			else if(strcmp(operation,"Operation2")==0)
				paramNo=2;
		
			else if(strcmp(operation,"Operation3")==0)
				paramNo=3;
			
			else if(strcmp(operation,"Operation4")==0)
				paramNo=4;
			
			/*Result*/
			if((fd_r = open(mathServerFifoName,O_RDONLY))==-1){
				fprintf(stderr,"Cannot open fifo: %s\nProgram is closing.\n",fifoName);
				return -1;
			}
			while(read(fd_r,buffer,255)>0);
			fprintf(reportFile,"%s\n",buffer);
			close(fd_r);
			
			
			k++;
			if(((argc-4)% paramNo !=0 || k>=((argc-4)/paramNo)))
				break;
		}
		unlink(mathServerFifoName);
		fclose(reportFile);
		
	}

}


void used(){
	fprintf(stderr,"Usage:\n\t./client <mainFifoName> <waitingTime> <operationName> <param1> <param2>...<paramk>\n",
	"Max param count 12\n");
}
