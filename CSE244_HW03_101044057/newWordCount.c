/**
*	@author Hakan DEGIRMEN Num:101044057
*	@version v.1.0
*	@since 03.04.2015
*/

/**
*	         COMPILER & RUN
*		_________________________
*	
*	*	gcc -c newWordCount.c
*  	*	gcc wordCount.o -o newWordCount
*	*	./newWordCount <directoryName>
*
***/


/****************************************************
*					KUTUPHANELER
*****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

/**
*	typedef struct newWord bu structure kelime ve kac tane oldugunu tutar
*	@param word kelime
*	@param number o kelimeden kac tane oldugu
**/



typedef struct newWord{
	char word[255];
	int number;

}newWord;

/**
*	Bu fonksiyon verilen pathdeki
*	tüm klasor ve dosyaları bulma
*	ya yarar.
*
	*@param string Path
**/
void dizinDolas(char *strDirName);

/**
*		Verilen pathin klasor olup olmadigini dönderir
*		@param string Path
*		@return int isDirectory path klasorse modunu degilse 0 donderir.
**/
int isdirectory(char *path);

/**
*	Verile file adında kelime sayacak olan fonksiyondur.
*	@param fileName kelime sayilacak dosyanin adi
*	
**/
void search(char *strDirName);

/**
*	Verilen bir satirda kac tane gecerli kelime var onu sayacal
*	@param string line  source line
*	@return int count of valid word
**/
int searchLine(char chSatir[]);


/**
*	Verilen kelimenin daha onceden var olup olmadigina bakar
*	Var ise listedeki indexi donderir yoksa -1 donderir
*	@param key string 
*	@return verilen stringin hangi indexde oldugunu gosterir yoksa -1 donderir
**/

int isContain(char key[]);
/**
*	Filede buldugu gecerli kelimeleri fifoya yazacak
*	@param fileName source file
*	@param fifoName target fifo Name
*	@return success
*
**/
int traceFile(char fileName[],char fifoName[]);


/**
*	Parentin okudugu kelimeleri bir newWord
*	structi halinde arraya doldurur 
**/
void fillArr();


/**
* 	Childin yazdigi fifo adini parametre olarak vererek parentin kelimeleri okumasina
* 	yardimci olan fonksiyon
* 	@param fifoName 
**/
void report(char fifoName[]);



/**
*			GLOBAL VARIABLES
**/
int wordSize=0;
int wordArraySize=0;
newWord wordArr[150005];
char fileNames[255][255];
int fileNumber=0;
int totalDirectory=0;


/**
*	Main Function Driver function
*	
*	@param int argc number of arguments   
*	@param char** argv  arguments
*	@return success
**/

int main(int argc, char ** argv) {


	char strDirName[255];
	int i;
	char strDir[255];
	pid_t childPid;
	char fifoName[255];
	FILE *reportFile;

	/*############################*
	#			USAGE
	##############################*/
	
	if(argc != 2){
		printf("Usage:./newWordCount <directoryName>\nProgram is closing...");
		return 0;
	}else{
		strcpy(strDirName,argv[1]);/*Argumanlari her ihtimale karsi kopyaliyorum*/	
		getcwd(strDir,255);		//get current working directory
		strcat(strDir,"/");
		strcat(strDir,argv[1]);//yeni directoryi ayarliyorum
		if(!isdirectory(strDir)){//directory yoksa kapatiliyor
			printf("Boyle bir directory yok program kapatiliyor.\n");
			return 0;
		}
		dizinDolas(strDir);//duzgun ise recursive bi sekilde icindekileri listeyeyecek
		
		for(i=0;i<fileNumber;++i){
			/*her bir dosya icin yeni bir fifo olusturuyorum*/
			sprintf(fifoName,"%s%d","fifo_",i);
			
			if (mkfifo(fifoName, S_IRUSR | S_IWUSR) == -1) {

	        	perror("failed mkfifo\n");
	        	unlink(fifoName);
	        	exit(2);
    		}
			childPid = fork();
			if(childPid==-1){

				fprintf(stderr,"Cannot create process\nProgram closing...");
				return 0;

			}
			else if(childPid==0){
				/*dosyadaki gecerli kelimeleri bulup verilen isimdeki fifoya yazacak*/
				traceFile(fileNames[i],fifoName);
				exit(0);/*Prosesi isi bittiginde olduruyorum */

			}else{
				/*Childin yazdigi fifoyu okuyup bilgileri kaydedecek*/
				report(fifoName);

			}
			strcpy(fifoName,"\0");
		}
		
	}
	/*Olusturdugum fifolari siliyorum*/
	for(i=0;i<fileNumber;++i){

		strcpy(fifoName,"\0");
		sprintf(fifoName,"fifo_%d",i);
		unlink(fifoName);
	}
	/*Bilgileri struct arrayine dolduruyorum*/
	fillArr();
	unlink("report.txt");
	/*Bilgileri report.log dosyasina kaydediyorum*/
	if((reportFile=fopen("Report.log","w"))==NULL){
		fprintf(stderr,"Cannot open Report.log file\nProgram is closing...\n");
		return 0;

	}
	for(i=0;i<wordArraySize;++i)
		fprintf(reportFile,"\t%s   ---------->%d\n",wordArr[i].word,wordArr[i].number);	
	
	printf("Total Directory: %d\nTotal File: %d\nTotal Child Proses: %d\nTotal Different Word: %d\n",totalDirectory,fileNumber,fileNumber,wordArraySize);
	fprintf(reportFile,"____________________________________________________________\n");
	fprintf(reportFile,"Total File:%d   Total Child:%d   Total Different Word:%d\n",fileNumber,fileNumber,wordArraySize);

	fclose(reportFile);

	return 0;
}/*main End*/



/**
*	Bu fonksiyon verilen pathdeki
*	tüm klasor ve dosyaları bulma
*	ya yarar.
*
*	@param string Path
**/

void dizinDolas(char *strDirName){

		struct dirent *direntp;
    	DIR    *dirp;
    	char   *strNewPath;
		char    *strFilePath;
		int 	iDirSize;
		char *strRoot;


		/*gerekli yerleri kopyaliyorum*/
		strNewPath=(char *)malloc(sizeof(char)*255);
    	strFilePath=(char *)malloc(sizeof(char)*255);
		strRoot=(char *)malloc(sizeof(char)*255);


		strcat(strNewPath,strDirName);
		/*directoryi aciyorum acilamazsa hata veriyorum*/
		if ((dirp = opendir(strDirName)) == NULL) {
				fprintf (stderr, "Failed to open directory \"%s\" : %s\n",
				         direntp->d_name, strerror(errno));

				exit(0);
		}
		while((direntp=readdir(dirp))!=NULL){//directoryde eleman kalmayana kadar okuma yapıyorum

			if(direntp->d_type & DT_DIR)//klosorse
			{
				
					iDirSize=strlen(direntp->d_name);/*dosya ad uzunlugunu alip .. veya . olmamasına dikkat ediyorum yoksa beni sürekli geri atar*/
					
					
					if(iDirSize==2 && direntp->d_name[0]=='.' && direntp->d_name[1]=='.'){
							
					}else if(iDirSize==1 && direntp->d_name[0]=='.' ){
						
					}else
					{	
						totalDirectory++;
						strcpy(strRoot,strNewPath);/*yeni klasorle beraber yeni pathi beirliyorum ve recursive bi sekilde bitene kadar yapiyorum*/
						strcat(strRoot,"/");
						strcat(strRoot,direntp->d_name);
						//printf("Klasor:  %s\n",strRoot);
						if(strlen(strRoot)<=255)/*Unix path uzunlugunu 255 den fazla izin vermedigi icin kontrol ediyorum*/
							dizinDolas(strRoot);	
						else{
							printf("Dosya isim boyutu asti\nProgram kapatiliyor.\n");
							break;
						}
				
					}
	
			}
			else{//dosya ise
				
				iDirSize=strlen(direntp->d_name);
				if(direntp->d_name[iDirSize-1] != '~'){
					strcpy(strFilePath,strNewPath);/*Yeni dosya ile beraber yeni path buluyorum*/
					strcat(strFilePath,"/");
					strcat(strFilePath,direntp->d_name);
					if(strlen(strFilePath)<=255){//arama fonksiyonunu gonderiyorum
						/*#######################################
						#	Dosya isimlerini arraya kaydediyorum
						#########################################*/
						strcpy(fileNames[fileNumber++],strFilePath);
					}
					else{
						printf("Dosya isim boyutu asti\nProgram kapatiliyor.\n");
						break;
					}					
				}
			}

		}
		free(strNewPath);//mallocla aldıgım yerleri geri veriyorum
		free(strFilePath);
		free(strRoot);
		if (closedir (dirp)) {//dosyayı kapatiyorum
			fprintf (stderr, "'%s:	dosyasi kapatilamadi %s\n",strDirName, strerror (errno));
	   		 exit (EXIT_FAILURE);		
 		}

}/*DizinDolas end*/




int traceFile(char fileName[],char fifoName[]) {

	FILE *file;
	char word[255];
	char *line;
	int offset,fd_w;
	int i=0;
	int status;
	

	/*Verilen dosyayi acip kelimeleri ayirip gecerli olanlari verilen fifoya yazar*/
	if((file=fopen(fileName,"r"))==NULL){
		fprintf(stderr,"Dosya acilamadi.%s\n",fileName);
		return -1;

	}
	if(!(fd_w = open(fifoName,O_WRONLY))){
		fprintf(stderr,"Cannot open fifo %s\nProgram is closing...",fifoName);
		return -1;
	}
	line =(char *)malloc(sizeof(char)*255);
	while(!feof(file) && fgets(line,255,file) != NULL){
		while(sscanf(line,"%s%n",word,&offset)==1){
			
			for(i=0;i<strlen(word);++i){
				if(word[i]>=65 && word[i]<=122){
					if(word[i]>=91 && word[i]<=96)
						status=-1;
				}else{
					status=-1;
				}

			}
			if(status!=-1){
				write(fd_w,word,255);/*Fifoya yaziyor*/
				wordSize++;
			}
			status=0;

			line+=offset;
			
		}
	}
	//free(line);// offset ekleye ekleye sona kadar geldigimiz icin free gereksiz
	close(fd_w);
	fclose(file);
	return 0;
	
}

void fillArr(){

	FILE *reportFile;
	char *line;
	int index;
	reportFile=fopen("report.txt","r");
	
	line=(char *)malloc(sizeof(char)*255);
	
	while(!feof(reportFile) && fgets(line,255,reportFile) !=NULL){
		line[strlen(line)-1]='\0';
		index=isContain(line);
		if(index != -1){

			wordArr[index].number++;
		}
		else{

			strcpy(wordArr[wordArraySize].word,line);
			wordArr[wordArraySize++].number=1;

		}
			
	}

	fclose(reportFile);
	free(line);
	

}


int isContain(char key[]){
	int i=0;
	for(;i<wordArraySize;++i){
		if(strcmp(wordArr[i].word,key)==0){
			return i;
		}

	}
	return -1;


}
int isdirectory(char *path) {
   struct stat statbuf;

   if (stat(path, &statbuf) == -1)
      return 0;
   else
      return S_ISDIR(statbuf.st_mode);
}

void report(char fifoName[]){

	int fd_r;
	char line[255];
	FILE *reportFile;
	reportFile=fopen("report.txt","a");
	if(!(fd_r = open(fifoName,O_RDONLY))){
		fprintf(stderr,"Cannot open fifo %s\nProgram is closing...",fifoName);
		exit(1);

	}
	while (read(fd_r,line,255)>0) {
        fprintf(reportFile,"%s\n", line);
    }
	close(fd_r);
	fclose(reportFile);
}



